Orphan users statement

               exec sp_change_users_login 'Update_one','msadmin','msadmin'

Insert msadmin directly into users table

               insert into stptup (login_id, password,plant_no) values ('msadmin','msadmin','000')
msadmin encrypted password

               update stptup set password = '2ip_cmjp' where login_id = 'msadmin'
set admin rights against a user

               update stptup set window_type ='A' where login_id = 'msadmin'
checking mainsaver database version

               select * from dbver
Reset a users password to 'password' in WM

               UPDATE users SET password = '5f4dcc3b5aa765d61d8327deb882cf99' WHERE username = '[username]';
Changes the EPRAIS user password to Passw0rd!

               UPDATE eprais_user SET password='81d8afa3eb8e985fb2eb19c3c65552107338b9549752bbeb83d3ae4ccc9bf9ab008b9abcc42de381' WHERE username = 'admin'

Create SwipeCard Login In SQL will need to run the update statement as below against the WM User Database and the USERS table

              UPDATE users SET card_number = '0000' WHERE username = '[username]';
              
To create a number of SQL statements using data in a spreadsheet

              1) Copy the following into a cell next to the first set of data in the spreadsheet
                 ="update mtpm set avg_cost='"&D2&"' where stockno='"&A2&"';"
              2) Change the SQL command just copied to include the relevant table and fields (as you would write a normal SQL statement)
              3) Change the reference to cells to the relevant cells that hold the data in the spreadsheet, in the copied SQL command.
                 e.g. the original command above would be changed to: ="update ast set assetshortdesc='"&A2&"' where assetno='"&A3&"';"
              4) Once the SQL command is complete, select the cell and drag the bottom right corner of the highlighted cell to the bottom 
                 of the spreadsheet to the end of the list of data.
              5) The SQL commands should be copied down to create unique SQL statements for each line.
Find out the last executed sql in Oracle

             select sql_text from v$sql where first_load_time=(select max(first_load_time) from v$sql)
Check if there is a trigger on the database

             SELECT trigger_name = sysobjects.name, trigger_owner = USER_NAME(sysobjects.uid),table_schema = s.name, table_name = OBJECT_NAME(parent_obj),
 isupdate = OBJECTPROPERTY( id, 'ExecIsUpdateTrigger'), isdelete = OBJECTPROPERTY( id, 'ExecIsDeleteTrigger'),
 isinsert = OBJECTPROPERTY( id, 'ExecIsInsertTrigger'), isafter = OBJECTPROPERTY( id, 'ExecIsAfterTrigger'),
 isinsteadof = OBJECTPROPERTY( id, 'ExecIsInsteadOfTrigger'),
 [disabled] = OBJECTPROPERTY(id, 'ExecIsTriggerDisabled') FROM sysobjects INNER JOIN sysusers ON sysobjects.uid = sysusers.uid
            INNER JOIN sys.tables t ON sysobjects.parent_obj = t.object_id
           INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
         WHERE sysobjects.type = 'TR'
if link between db and filename is lost

               ALTER DATABASE [MyDatabaseName] SET MULTI_USER
Enable/Disable All triggers

               sp_msforeachtable "alter table ? disable trigger all"
               sp_msforeachtable "alter table ? enable trigger all"
Delete duplicate row

               WITH CTE AS(
  SELECT [corr_prefix], [corr_counter],
      RN = ROW_NUMBER()OVER(PARTITION BY corr_prefix ORDER BY corr_prefix)
  FROM dbo.stptnum
) DELETE FROM CTE WHERE RN = 2

To find a carriage return in a particular field:

               SELECT * FROM mtpm WHERE description like '%'+char(13)+'%' or description like '%'+char(10)+'%'
To replace a carriage return in a particular field:

               UPDATE mtpm
               SET description = Replace (description, CHAR(13), ' ')
               WHERE description like '%'+char(13)+'%' or description like '%'+char(10)+'%'
Find variations of a procfile

               select * from (
               select procfile,COUNT(upper(ltrim(rtrim(procfile)))) 
               over (partition by upper(ltrim(rtrim(procfile)))) proc_count
               from (
               select distinct ASCII(procfile) asc_proc, procfile from mtpmp
               ) proc_table_dist ) windowed_count
               where proc_count > 1
Update a WM card number to match e_opt1 in EM

               update users 
               set card_number = e_opt1
               FROM Mainsaver_live.dbo.em 
               join wm_users.dbo.users  
               ON em.empl_id 
               collate SQL_Latin1_General_CP1_CI_AS=users.username