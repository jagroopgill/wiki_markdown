# Customer License Keys


### Allied Bakeries
<pre>
Customer Number:   
Mainsaver License Key:  
Pentaho License On Network: 
</pre>

### ALPS Electric
<pre>
Customer Number: 3955  
Mainsaver License Key: 115524P8DCLHQ328380029  
Pentaho License On Network: K:\Customers\ALPS Electric 3955\Pentaho License Files  
Pentaho License Expiry Date: 30/06/15  
</pre>

### Amtico
<pre>
Customer Number: 4008  
Mainsaver License Key: 1032466XDCLU6727170059  
Pentaho License On Network:  
Pentaho License Expiry Date:  
</pre>

### Archimica
<pre>
Customer Number:   
Mainsaver License Key:   
Pentaho License On Network: K:\Customers\Archimica  
Pentaho License Expiry Date:  
</pre>

### Associated British Ports
<pre>
Customer Number: 3936  
Mainsaver License Key:  
Pentaho License On Network:   
</pre>

### Bakeaway
<pre>
Customer Number: 4200  
Mainsaver License Key: 1262468SD2LQB728490029CKK  
Pentaho License On Network: 
</pre>

### Belfast Health & Social Care Trust
<pre>
Customer Number: 3977  
Mainsaver License Key: 10234P26D0LYH626800025  (customer number 3914)  
 115424P6DCL4Y326860119    
Pentaho License On Network:
</pre>

### Bespak
<pre>
Customer Number: 4189
Mainsaver License Key: 126M55P6D2L2Q327270029CKK  
      Pentaho License On Network:
</pre>

### Bio Products
<pre>
Customer Number: 4017  
Mainsaver License Key: 116X2468D0LBQ325260029          Pentaho License On Network: K:\Customers\BioProducts
</pre>

### Bowmans
<pre>
Mainsaver number: 4130  
Mainsaver License Key: 116Y235790L3Q322260029  
</pre>

### Caparo
<pre>
Customer Number: 3959  
Mainsaver License Key:   
Pentaho License On Network:   
</pre>

### Caterpillar
<pre>
Customer Number: 4030  
Mainsaver License Key: 1032452890RYJ625060029  
Pentaho License On Network:   
</pre>


### CBS
<pre>
Customer Number: 3910  
Mainsaver License Key:   
Pentaho License On Network: K:\Customers\CBS 3910\Pentaho License Files  
Pentaho License Expiry Date:
</pre>

### Cranberry Foods
<pre>
Customer Number: 4151
Mainsaver License Key: 116F24P8DRLBU330270025
Pentaho License On Network:
Pentaho License Expiry Date:
</pre>

### Culham
<pre>
Customer Number: 4000
Mainsaver License Key: 102R3456DCRLU328960159
                     : 103245D6DCRUL628800155
                     : 116W2466DCLBU327400159
Pentaho License On Network: 
</pre>

### Dawnfresh
<pre>
Customer Number: 4043
Mainsaver License Key 10.3: 1032J4P6DCL2Q426520029
Mainsaver License Key 11.5: 1152J4P6DCL2Q426560025
Pentaho License On Network: 
K:\Customers\Dawnfresh\Pentaho License Files
</pre>

### De La Rue
<pre>
Customer Number: 
Mainsaver License Key:
Pentaho License On Network: 
</pre>

### Denby Pottery
<pre>
Customer Number: 
Mainsaver License Key:
Pentaho License On Network: 
</pre>

### Denso Marston
<pre>
Customer Number: 3989
Mainsaver License Key: 1162J4689CL4Q424350029
Pentaho License On Network: 
</pre>

### Derwent Cogeneration
<pre>
Customer Number: 9096
Mainsaver License Key:
Pentaho License On Network: 
</pre>

### Dover Habour Board
<pre>
Customer Number: 3952
Mainsaver License Key: 102R34P890L2U324490025
Mainsaver License Key: 115424P6D0LFY327050029
Pentaho License On Network: K:\Customers\Dover Harbour Board 3952\Pentaho Licence Files
</pre>

### Dublin Airport
</pre>
Customer Number: 3949
Mainsaver License Key: 10355466DCR2Q324560025
Pentaho License On Network: K:\Customers\Dublin Airport 3949\Pentaho License Files
</pre>

### Faccenda
<pre>
Customer Number: 3951
Mainsaver License Key: 
Pentaho License On Network: K:\Customers\Faccenda 3951\Pentaho License Files
</pre>

### Fresh Linen
<pre>
Customer Number: 4197
Mainsaver License Key: 12624PS6DRLQ1630750029CKK
</pre>

### Fujifilm
<pre>
Customer Number: 4212
Mainsaver License Key: 12854PD6D2LU1627020029CKK
</pre>

### Ginsters
<pre>
Customer Number: 3969
Mainsaver License Key: 
Pentaho License On Network: K:\Customers\Ginsters 3969\Pentaho License Files
Pentaho License Expiry Date: 23/07/11
</pre>

### Global Renewables
<pre> 
Customer Number: 4016
Mainsaver License Key: 
Pentaho License On Network: K:\Customers\Global 4016\Pentaho license key file
license key to expire on the 03/11/16 at 23:59
</pre>

### Greencore Convenience
<pre>
Customer Number: 4004
Mainsaver License Key: 11655P6XCCLUH731170109
Pentaho License On Network: K:\Customers\Greencore Convenience 4004\Pentaho License Files
</pre>

### Greencore Malt
<pre>
Customer Number: 3960
Mainsaver License Key: 102W65P6DCL3U327740025
Pentaho License On Network: 
</pre>

### Highland Spring
<pre>
Customer Number: 4214
Mainsaver License Key: 1282N4P6D2L1Y427990089BK2
Pentaho License On Network:
</pre>

### Huntapac
<pre>
Customer Number: 
Mainsaver License Key: 
Pentaho License On Network: K:\Customers\Huntapac 4062\Pentaho Licence Files
license key to expire on the 03/11/16 at 23:59
</pre>

### Ideal Boliers
<pre>
Customer Number: 3921
Mainsaver License Key: 
Pentaho License On Network: 
</pre>

### Adams Food Limited (formerly Kerrygold)
<pre>
Customer Number: 3920
Mainsaver License Key: 
Pentaho License On Network: 
</pre>

### Harwell and Winfrith
<pre>
Customer Number: 4001
Mainsaver License Key: 
Harwell: 102R3456DCRLU328970305
Winfrith:
Pentaho License On Network: 
</pre>

### Kimberely-Clark
<pre>
Customer Number: 3982
Mainsaver License Key: 
Pentaho License On Network: 
</pre>

### Knauf Immingham
<pre>
Customer Number: 3957
Mainsaver License Key: 
Pentaho License On Network: 
</pre>

### Knauf Sittingbourne
<pre>
Customer Number: 3956
Mainsaver License Key: 1152N466DCLFY428500029
Pentaho License On Network: 
</pre>

### Leeds Bradford
<pre> Customer Number:
Mainsaver License Key:
Pentaho License On Network: 
</pre>

### Lightbody
<pre>
Customer Number: 4178
Mainsaver License Key: 11624P6SDRLQF731570029
Pentaho License On Network: 
</pre>

### London And Scandanavian
<pre>
Customer Number: 4035
Mainsaver License Key: 1032I45890RLU426160055
Pentaho License On Network: 
</pre>

### Macdermid Autotype
<pre>
Customer Number: 4158
Mainsaver License Key: 12612468D2LHY327920025CKK
Pentaho License On Network: 
</pre>

### Marshall Aerospace
<pre>
Customer Number: 4026
Mainsaver License Key:
Pentaho License On Network: 
</pre>

### Mersey Tunnels
<pre>
Customer Number: 
Mainsaver License Key:
Pentaho License On Network: 
</pre>

### Newly Weds
<pre>
Customer Number: 4102
Mainsaver License Key: 116R2466DCL2Q324790029
Pentaho License On Network: K:\Customers\Newly Weds Foods
</pre>

### Pegler
<pre>
Customer Number: 4046
Mainsaver License Key: 115M246890LDQ324070109
                       103M246890LDQ324030109
Pentaho License On Network: 
</pre>

### Queens College
<pre>
Customer Number: 3935
Mainsaver License Key: 
Pentaho License On Network: 
</pre>

### Royal Hospital Trust
<pre>
Customer Number: 3912
Mainsaver License Key: 
Pentaho License On Network: 
</pre>

### Sandvik
<pre>
Customer Number: 4104
Mainsaver License Key:1162468S90LQD725390029
Pentaho License On Network: 
</pre>

### Shannon Airport
<pre>
Customer Number: 3967
Mainsaver License Key: 10355466DCR2Q324560029
Pentaho License On Network: 
</pre>

### Simpsons
<pre>
Customer Number: 3914
Mainsaver License Key: 12625SP6DCLYF532910029CKK
</pre>

### Sita
<pre>
Customer Number: 4034
Mainsaver License Key: 103X246890L6U323280025
Pentaho License On Network: K:\Customers\Sita 4034\Pentaho license files
license key to expire on the 03/11/16 at 23:59
</pre>

### Symphony
<pre>
Customer Number: 3943
Mainsaver License Key: 1032N4P8DCR2Y428660155
Pentaho License On Network: K:\Customers\Symphony 3943\Pentaho License Files
</pre>

### Tamar Foods
<pre>
Customer Number: 4040
Mainsaver License Key: 116X25P6DCLFQ329320079
                       115425P6DCL2Y326500075
                       103425P6DCL2Y326460079
Pentaho License On Network: K:\Customers\Ginsters 3969\Pentaho License Files
       - They share the BI license with Ginsters as Pentaho is licensed to "Samworth Brothers"
</pre>

### Uniq
<pre>
Customer Number: 4013
Mainsaver License Key: 103R24689CL2Y324950159
Pentaho License On Network: 
</pre>

### Warburtons
<pre>
Customer Number: 3983
Mainsaver License Key: 
Pentaho License On Network: 
</pre>

### Warwick Chemicals
<pre>
Customer Number: 
Mainsaver License Key: 
Pentaho License On Network: K:\Customers\Warwick
license key to expire on the 14/06/17 at 23:59
</pre>

### Weetabix
<pre>
Customer Number: 3942
Mainsaver License Key: 
Pentaho License On Network: K:\Customers\Weetabix 3942\Pentaho-Solutions
</pre>

### Westmill
<pre>
Customer Number: 4063
Mainsaver License Key: 126M24P6DCLFQ331070029CKK
Pentaho License On Network:
</pre>

### Wirral Hospital
<pre>
Customer Number: 4006
Mainsaver License Key: 102M2466D0RDY326500059
Pentaho License On Network: 
</pre>

### Wiseman
<pre>
Customer Number: 3917
Mainsaver License Key: 116X25P6DCLFQ329440205
Pentaho License On Network: K:\Customers\Wisemans 3917\Pentaho License Files
Pentaho License Expiry Date: 
</pre>

### Wolf Minerals
<pre>
Customer Number: 4211
Mainsaver License Key: 12655SP6C2LY45286500293KK
Pentaho License On Network: K:\Customers\Wood Group 4071\Pentaho License Files
</pre>

### Wyeth
<pre>
Customer Number: 3963
Mainsaver License Key: 
Pentaho License On Network: 
</pre>

### Woodgroup
<pre>
Customer Number: 4071
Mainsaver License Key: 128F2468D2L4Y326340029CKK
Pentaho License On Network: K:\Customers\Wood Group 4071\Pentaho License Files
</pre>