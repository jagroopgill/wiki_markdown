# FTP Logins

Connection

External Host ftp.spidex.co.uk

Example URL for a browser ftp://username:password@ftp.spidex.co.uk

Notes

We recommend that an ftp client such as FileZilla Client is used at all times.

ftp.spidex.co.uk may not work internally, in which case use 10.0.1.1 as the hostname

Connecting to FTP using DOS

The login for our FTP is below. You can access this via ftp.spidex.co.uk 

Username:   
Password:   

Customer Logins  
Site |	Username   |	Password  
---|---|---
Adams Foods |	adamsfoods|	hiJ*b1Glp!  
AG Barr	| agbarr	| Ma1n$aver  
Allied Bakeries	| allied |	hs&nbi!g6t  
Alps |	alps |	8iK$^fdhsjk
amg	| amg |	Agadsgtf!
Archimica |	Archimica | Bigh7f^r
Associated British Ports |	abports |	Y7^se2b*
Belfast |	belfast |	K74e5b!
Bespak |	bespak |	be$pakm
Bio Products |	bpl |	De1rt^r5
Birmingham Airport |	bhx |	BHg6%ds
Caparo |	caparo	| J^bk9Cw*
CBS | 	cbs	| H7sNk54p
CCFE |	ccfe |	Pa$$w0rd
Contract Support Consultancy |	consc |	Zz34Ja72
CPS |	cps |	C!p£gs1d
Cranberry Foods |	cranberry |	C!r@nB3rry
DAA	 | daauth | 	G%hg97i^g
Dawnfresh |	dawnfresh |	D@wn4r3sh!
Denso |	denso |	M@r$ton!
Dover Harbour Board	| dover	| Nt84Ap78
Ebest |	ebest |	Sf4vf*sd
Elizabeth The Chef |	ETC	 | mj*r2rc9!x
Faccenda |	faccenda |	Ch1c8Msr
Finsbury Foods |	finsbury |	Fbh736!s
Gdansk	 | Gdansk |	H7435uF^
Ginsters |	Ginsters |	P45t13s£
Global Renewables |	global |	R3n3w^B1s
Greencore |	greencore |	Yc33Pq99
Harvest |	harvest |	H@r!v5t
Highland Spring |	highlandspring |	Mj8h*sq18j
Huhtamaki |	huhtamaki |	Hi8h*sq18a
huntapac |	huntapac |	Nj7h%sq1
Ideal Boilers |	3921 |	Ma99Ys0P
Ideal Boilers |	ideal |	i8b&dw^z
Infomaker |	infomaker |	1nF0M4ker!
Infomaker125 |	infomaker125 |	1nF0M4ker!125
Jas Bowmans |	bowmans |	B1wMa5z!
knauf |	knauf |	F1f^hysa8
Kloeckner |	kloeckner |	%^dfjs2d09!
Knighton Foods |	knighton |	Kn1ght0n
Leeds |	leeds |	Lsgh23%d
LSM |	LSM |	Lsgh23%d
Lyons |	Lyons |	Lgs8^4as
MacDermid |	macdermid |	Mhybhd6!4
Mainsaver Support |	msvr_support |	m$vR_$upp0rt
Marshalls |	marshalls |	y+b&8lS0
Mass Global |	mass_global |	M@ss_Gl0bal
Mercy |	mercy |	Hsd23u&h
Mersey |	mersey |	M2&sfyrt9
MobileMainsaver |	mobile |	Ma1ns4ver
Molson Coors |	molsoncoors |	M0lsCo0r$
Muller |	muller |	Mahf&1sd
NewlyWeds Food |	newlyweds |	H54y&ga
Nufarm |	nufarm |	dgh&%*_iu
ornua |	ornua |	ornua483
pegler |	pegler |	JGj%98h_p
Pete's folder |	pete |	Hngjs7$bhf
Prysmian |	prysmian |	P9v^89h
Royal Hospital |	3912 |	Ms65JwMz
RSRL - Harwell |	harwell |	H*^gfdM%3
Saladworks |	saladworks |	Sa1md6l!2
Sandvik |	Sandvik |	Mjh*dg71
Shannon |	shannon |	Hg6F2*fs
Sheffield Forgemasters |	forgemasters |	Fhnsjk%f
Sika |	sika |	Snmerh23
Simpsons |	3914 |	Jw4z6y9L
Sita |	sita |	S8fuios
Smith & Nephew |	smith&nephew |	Ybgrw!3prv
Symphony Group |	symphony |	SymPh0ny1!
Tamar |	tamar |	Lh7ufh£d
TechGroup |	techgroup |	tsgs!sC4
Thermofisher |	thermofisher |	Tm0%F5er&
Thistle SeaFoods |	thistle |	Seaf00ds
Twinings |	twinings |	T34o^algH*m
Unipart |	unipart |	M7Ghr!gy
Warwick |	warwick | 8a54ingP£Ng
Weetabix |	weetabix |	B1sk$1m9^
Westmill Foods |	westmill |	P1sw3^bc
Wirral |	wirral |	h7RaD6
Wiseman |	wiseman |	G2xza77p

General Logins

Description |	Username |	Password
---|---|---
Spidex staff account |	spidex |	Ma1nsaveR
All others (non staff or defined customer acct)	|external |	N1Jwhs9^Msr
Mainsaver Client Installer |	msvr_fco |	M5vR_Fco1
Mainsaver all installs |	msvr_ais |	M5Vr_AiF9
Powerbuilder 10 |	powerb10 |	P0w3R&10
Infomaker 10 |	info10 |	1nFom$10

Drag & Drop Files