# Adding a New Tomcat

Whether it is a new isis/datasabre/wm install a new tomcat will need to be added to the server, these are the steps.

Please follow these exactly so that we can ensure that each install is identical.

NOTE : Any changes to Apache will require a restart, so you will need to schedule downtime to bring the new tomcat in. This is only on Windows, if this becomes a problem we will need to use unix servers to run apache

### Directory Setup for Tomcat Unzip
<pre>
c:\apps\Apache Tomcat\
</pre>

Has the install zip and the setenv file which is already configured with the correct JAVA_HOME and catalina opts settings

Make sure that the tomcat goes into the correct place depending on the application you are setting up

<pre>
c:\Apache_Tomcat\datasabre
c:\Apache_Tomcat\isis
c:\Apache_Tomcat\wm
</pre>

Create a folder in those based on the customer name, e.g. bhstc, mwd etc. This name should be no more than 5 characters as it is also used for part of the service name Unzip the apache zip directly into the new folder and copy in the setenv.bat file

DO NOT START IT UP YET

### Configuring server.xml - Initial Install and Internal Testing

Take a copy of

<pre>
c:\Apache_Tomcat\cust_name\conf\server.xml
</pre>

and save as server.xml.original

Open the Hosted Tomcat Setups file.

Add a new line following exactly the same naming conventions as the previous lines. Ensure that the port numbers have been increased by 1 from the previous line. Commit the changes back to subversion. By keeping the port numbers increasing in this fashion we are ensuring there are no port conflicts on startup of apache. The port numbers are high as they are in the dynamic range which are not used by other software see - Default Dynamic Port Range for Windows Servers

Open server.xml in notepad++ and make the following adjustments (note the following ports are for the demo datasabre install as depicted in the Hosted Tomcat Setups file

Change the shutdown port from

<pre>
&lt;Server port="8005" shutdown="SHUTDOWN">
</pre>

to
<pre>
&lt;Server port="53000" shutdown="SHUTDOWN"> 
</pre>

Change the HTTP port from

<pre>
&lt;Connector port="8080" protocol="HTTP/1.1" 
  connectionTimeout="20000" 
  redirectPort="8443" />
</pre>
To
<pre>
&lt;Connector port="51000" protocol="HTTP/1.1" 
  connectionTimeout="20000" 
  redirectPort="54000" />
</pre>

Change the AJP Port from
<pre>
&lt;Connector port="8009" protocol="AJP/1.3" redirectPort="8443" />
</pre>
To
<pre>
&lt;Connector port="52000" protocol="AJP/1.3" redirectPort="54000" />
</pre>

Save the server.xml file and start tomcat. You should be able to see the tomcat configuration page from (Don't forget to use your port number)

<pre>
http://sqlv01:51000
</pre>

Now stop tomcat and configure for datasabre/isis/wm as normal. Once happy that this is funcitoning correctly rename the webapp to the same name used in the directory setup section (bhsct etc).

### Configuring server.xml - For SSL and Proxy
To allow the Apache Server to work as a reverse proxy we must add proxy information and an alias to the server.xml file.

Edit the server.xml file

Change

<pre>
&lt;Connector port="51000" protocol="HTTP/1.1" 
  connectionTimeout="20000" 
  redirectPort="54000" />
</pre>
To
<pre>
&lt;Connector port="51000" protocol="HTTP/1.1" 
  connectionTimeout="20000" 
  maxThreads="150" maxHttpHeaderSize="8192" minSpareThreads="25" maxSpareThreads="75"
  enableLookups="false" acceptCount="100" disableUploadTimeout="true" URIEncoding="UTF-8"
  redirectPort="54000" proxyName="datasabre.spidexsaas.co.uk" proxyPort="443" secure="false" scheme="https"/>
  </pre>

At the bottom of the file add a server alias to the Host defintion changing
<pre>
      &lt;/Host>
    &lt;/Engine>
  &lt;/Service>
&lt;/Server>
</pre>

To
<pre>
       &lt;Alias>datasabre.spidexsaas.co.uk&lt;/Alias>
      &lt;/Host>
    &lt;/Engine>
  &lt;/Service>
&lt;/Server>
</pre>

Where the alias is the url for the app being installed.

Restart tomcat and complete the next section

### Configure httpd.conf - adding a new proxy path
Be very careful at this point. All of our tomcats are currently proxied through the same Apache Server, so if you break it you break all the deployed connections!

Take a copy of
<pre>
c:\Apache_Server\conf\httpd.conf
</pre>
and rename to
<pre>
c:\Apache_Server\conf\httpd.conf.todaysdate_time (ie 20140204_1351)
</pre>

Now edit httpd.conf. We only need to change one of the virtual hosts. The one you need to change will have ServerName set to the same as the Alias set in the server.xml

Scan down to the section that starts ProxyPass like this
<pre>
ProxyPass /demo http://sqlv01:51000/demo
ProxyPassReverse /demo http://sqlv01:51000/demo
</pre>
Copy those two lines, set the paths to your webapp name (should be the same as the cust name used for the directory setup) and make sure the port is the tomcat http port so
<pre>
ProxyPass /demo http://sqlv01:51000/demo
ProxyPassReverse /demo http://sqlv01:51000/demo
        
ProxyPass /bhsct http://sqlv01:51003/bhsct
ProxyPassReverse /bhsct http://sqlv01:51003/bhsct
</pre>
We still have the original definition so demo still works, and how there is one for bhsct.

Save the file and restart Apache, not forgetting that restarting apache must be done in downtime