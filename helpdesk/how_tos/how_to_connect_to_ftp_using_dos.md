# Connecting to FTP using DOS

In the command prompt enter:  

ftp ftp.spidex.co.uk
 
Enter the customers username and password used to logon to the ftp site (check the wiki for ftp logins)
 
To retrieve a list of files enter:

ftp>ls

To determine the location of where a file will be saved enter:

ftp>lcd

To retrieve a file from the ftp folder enter:

ftp>get “filename”

Where “filename” is replaced by the name of the file.  Example shown below:
 
To place a file onto the ftp site enter:

ftp>put “filename”

Where “filename” is replaced by the name of the file.  

To exit the command prompt enter:

ftp>quit

Note:  When logging onto the ftp from the ABP server you will need to use a proxy to access the ftp.  So the commands to connect to the ftp are as follows:

                D:
                Cd ftp*
                ftp aips-ftpproxy

username

                abports@ftp.spidex.co.uk

Password

                Z9asb34n


