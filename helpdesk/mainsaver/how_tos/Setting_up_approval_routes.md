When setting up approval routes, ensure the employee has been set up in the Personnel module as a PR Approver:

Setup apporval routes 
image1.jpg

In System Admin select Reference > Costs Centers/Approvers Route List.

Select the Assign icon and the following screen should appear:

Add a new Route selecting the Add button.

Select the Approvers to PR Routes tab where on the right should be a list of approvers, and on the left the list of routes.

Drag and drop employee names onto the new route created.

Setup apporval routes 
image2.jpg

If you select the E-mail notification check box at the bottom, the Approver should be automatically notified when the Purchase Request is made.

If you want to assign cost centres to the route select the CC to PR Routes tab and drag and drop the cost centres to the route.

Setting up other e-mail notifications

If you would like to notify the person that made the purchase request that it has been approved, you can set up Event Subscriptions in System Admin.

Make sure your employees have e-mail addresses set up in their Personnel record under the Detail tab.

Select Settings > Event Subscriptions

Setup apporval routes 
image3.jpg

Enter an Event (in the picture above 310 represents Purchase Request Approved), then you can enter a User Role. In this case an e-mail will be sent to the person who had requested it.

Setup apporval routes 
image4.jpg