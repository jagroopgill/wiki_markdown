#  Creating a Blank Database

## Option 1 - Using the database install tool

1) Locate the MsTblSpc.sql and SQLInstall.sql file in following location: Mainsaver\11.6\database

2) Edit the MsTblSpc.sql file (if needed) and change references to MSDB1 to the name of the database you are creating, as well as the file references to point at database files on your machine.

3) Open Database Install Tool and go through wizard. When it asks for the driver create an ODBC driver for the new database (just set it so it uses the master database as the default) so it can be selected here.

4) On the screen that allows you to run a SQL script, ensure the MsTblSpc.sql file is selected at the top.

5) When script has completed, then run the SQLInstall.sql file (you may have to edit the top line of this file first where it says 'Use MSDB1' and replace with your database name.


## Option 2 - Using SQL Server Management Studio

1) Create a new database to create the tablespace.

2) Take the commands from MsTblSpc.sql which refer to roles and run these commands against the new database.

3) Run the SQLInstall.sql script against the new database.