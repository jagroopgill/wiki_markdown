Shop Floor Module Setup

You need to be logged in as someone with rights to modify the registry  
Login to mainsaver and system administration  
Go to application switches  
Shop tab – ensure the Automatic Shop Logon is off
Shop floor setup 

image1.jpg

Log out of  mainsaver completely.  
Log back into mainsaver using the username and password (Possibly a login specific for that terminal.   
You can then set the default work queue for that user to be specific to the area around the terminal) for the default database connection for that machine.  
Click on Shop Floor  
You should be presented with

Shop floor setup 

image2.jpg

Login again with the same username and password, ensure you set the save as defaults  
Login to Shop floor work queue, make sure you can get to the menu.  
Log all the way out again (including the main menu)  
Log back into mainsaver and return to the shop tab of application switches in System Admin and turn the Auto Logon switch on.  
Log out all the way out of mainsaver again.  
Then create a short to C:\Program Files\Mainsaver\10.1\bin\msvrsf101.exe  
Target should read "C:\Program Files\Mainsaver\10.1\bin\msvrsf101.exe"  
Change the start in to "C:\Program Files\Mainsaver\10.1\3912" – customer number
Shop floor setup 

image3.jpg

Click on the short cut and you should go straight through to the shop floor work queue.