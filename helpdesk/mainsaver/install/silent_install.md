#  Silent Install

If you run the below script in command prompt and replace these sections with information relevant to yourselves then this would run a silent install


MSIEXEC.EXE /I "\\servername\Software\Mainsaver\11.6\MAINSAVER 11.6.MSI" ALLUSERS="1" CUSTNO="customer_id" SERIALNUMBER="Serial_no" Username1="Company_name" MYWORKINGDIR="%PROGRAMFILES(X86)%\mainsaver\11.6\bin" /QN /lv "C:\MS11.log"


Afterwards, an ODBC connection will have to be set up either manually or through the registry