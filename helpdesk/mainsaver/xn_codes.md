# XN Codes

XN Code	XN Description  
MT2	Stocked Item Issue All  
MT21	Issue WO to Stocked Item  
MT22	Issue Empl to Stocked Item  
MT23	Issue Asset to Stocked Item  
MT24	Issue CC to Stocked Item  
MT25	Issue Mtl Request to Stocked Item  
MT3	Non Stocked Item Issue All  
MT31	Mtl Release to a WO  
MT32	Mtl Release to an Empl  
MT33	Mtl Release to an Asset  
MT34	Mtl Release to a CC  
MT35	Mtl Release to a Mtl Request  
MT4	Reciepts All	  
MT41N	Receive Stock that is not owned  
MT41O	Receive Stock that is owned  
MT42N	Receive Non-Stocked that is not owned  
MT42O	Receive Non-Stocked that is owned  
MT43	Receive Services  
MT44	Reciept to a Work Order  
MT5	Returns All	  
MT51	Return Stocked Items  
MT52	Return to Supplier  
MT6	Transfers All	  
MT61	Transfer Stock  
MT62	Transfer Non-Stocked  
MT7	Material Adjustments All  
MT71D	Material Adjust Detail before  
MT71H	Material Adjust Header before  
MT72D	Material Adjust Detail after  
MT72H	Material Adjust Header after  
MT95	Material Adjust Value Change  
MT95D	Item Value Edit Detail  
MT95H	Item Value Edit Header    
PP01D	PO Add Detail    
PP01H	PO Add Header  
PP02D	PO Edit Detail before  
PP02H	PO Edit Header before  
PP03D	PO Edit Detail after  
PP03H	PO Edit Header after  
PP04D	PO Delete Detail  
PP04H	PO Delete Header  
PP05	PO Close	  
PP41	PO Receive Stocked  
PP42	PO Receive Non-Stocked  
PP43	PO Receive Services  
PP81	PO Approve	  
PP82	PO Request Approve  
PT10	PO re-open	  
XT91	Misc Credit/Charge  
MT95H	Item Value Edit Header    
PP01D	PO Add Detail  
PP01H	PO Add Header  
PP02D	PO Edit Detail before  
PP02H	PO Edit Header before  
PP03D	PO Edit Detail after  
PP03H	PO Edit Header after  
PP04D	PO Delete Detail  
PP04H	PO Delete Header  
PP05	PO Close	  
PP41	PO Receive Stocked  
PP42	PO Receive Non-Stocked  
PP43	PO Receive Services  
PP81	PO Approve	  
PP82	PO Request Approve  
PT10	PO re-open	  
XT91	Misc Credit/Charge  