# Helpdesk Responsibilities

1) Ensure helpdesk calls are answered within 2 rings - at times reception calls or another colleagues phone may need to be answered, if after 2 rings no-one answers please answer on behalf of your colleague and take a message.

2) Answer the phone in a clear, professional manner.

3) Ensure calls/e-mails etc are logged in Callmanager.

4) Ensure issue is logged in JIRA with all relevant information and in correct status - ensure security level set and not in the Open status.

5) Ensure wiki is updated if there are any changes in information. Any additional knowledge is encouraged to be placed on the wiki.

6) Make sure old issues are chased to avoid old issues remaining on your workload - send an e-mail saying you will assume the issue is closed if you don't hear by a certain date to prompt the customer to reply or to clear off your list.

7) Any issues that are reported by e-mail/JIRA, if you are manning support on your own these need to be picked up asap.

8) Respond to any customer correspondence within 2 hours.