# Helpdesk Call Script

 Call Sequence	|   What to say 
---|---
Introduction/Answering the phone | Good morning Spidex Helpdesk, how can I help you?
Welcome the responsibility | Yes, I can help you with that.
Who are you talking to?  | So I can look you up on the system, can I take your full name and company please? <br>  Could I take your email address?
Check spelling if not sure. | Would you mind spelling that please?
Summarise the issue to show you understand | OK, just so I’m clear, the problem you’re having is.......<br>(summarise issue)......<br>is that correct?
Explain what the caller needs to do	| The best way for me to help fix that problem is to see a screenshot of the error message. <br>Could you email that to me and I can start working on it straight away? <br> The best way for me to help fix that is to go through it with you over the phone. Are you next to your computer now?
Explain what happens next | OK, I’ve logged your call on the system. <br> As soon as I get your email I’ll get to work on the problem. <br> I’ll keep you updated on progress.
Wrap up the call | Is there anything else I can help you with?
Say goodbye | Thank you for calling. Goodbye.

