# Apache Server Setup

## Installed on SQLV01

<pre>
c:\Apache_server
</pre>

Must use the Apache install with SSL support included. The backup of this file is on dropbox for nicole.stevens@gmail.com in Work Backups\Cloud Backup\Insalled Software  

## SSQL Key
Was generated by Umbee, they also currently own the records for admin@spidexsaas.co.uk which was used to verify the ownership
keys are installed to the following server. Backup copies are on dropbox for nicole.stevens@gmail.com in Work Backups\Cloud Setup

<pre>
c:\Apache_Server\ssl_certs
</pre>

They are referenced within the

<pre>
c:\Apache_server\conf\httpd.conf file as the SSL certificates for the virtual hosts
</pre>

## HTTPD File setup
Here are the changes made to the httpd file for setup of proxy & ssl

### =Listen 443
Deleted the listen for 80 and added 443

<pre>
Listen 443
</pre>

### Uncommented\commented modules
The top two have been commented following an article on hardening Apache. The rest are required for SSL/proxy

<pre>
#LoadModule autoindex_module modules/mod_autoindex.so
#LoadModule include_module modules/mod_include.so
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_connect_module modules/mod_proxy_connect.so
LoadModule proxy_http_module modules/mod_proxy_http.so
LoadModule ssl_module modules/mod_ssl.so
</pre>

### Proxy and SSL Config
THis will evolve as more sites are added, however the basis is this:-
This section turns of notification that the server is a tomcat server. This is part of the hardening process

<pre>
ServerSignature Off
ServerTokens Prod
</pre>

This part states that the server will accept any domain on 443 and try and deal with it

<pre>
NameVirtualHost *:443
</pre>

Need to create a seperate virtual host for each domain (wm. , datasabre. , isis.)

<pre>
&lt;VirtualHost *:443&gt;
        ServerName datasabre.spidexsaas.co.uk
        ProxyRequests Off
        ProxyPreserveHost On
       
        &lt;Proxy *&gt;
                Order deny,allow
                Allow from all
        &lt;/Proxy&gt;
        ProxyPass /demo http://sqlv01:51000/demo
        ProxyPassReverse /demo http://sqlv01:51000/demo
       
        ProxyPass /bhsct http://sqlv01:51003/bhsct
        ProxyPassReverse /bhsct http://sqlv01:51003/bhsct
       
        SSLEngine On 
        SSLCertificateFile C:\Apache_server\ssl_certs\STAR_spidexsaas_co_uk.crt
        SSLCertificateKeyFile C:\Apache_server\ssl_certs\privatekey.key
        SSLCertificateChainFile  C:\Apache_server\ssl_certs\STAR_spidexsaas_co_uk.ca-bundle

        Redirect / http://www.spidexsoftware.co.uk/
       
&lt;/VirtualHost&gt;

&lt;VirtualHost *:443&gt;
        ServerName isis.spidexsaas.co.uk
        ProxyRequests Off
        ProxyPreserveHost On
       
        &lt;Proxy *&gt;
                Order deny,allow
                Allow from all
        &lt;/Proxy&gt;
        ProxyPass /demo http://sqlv01:51001/demo
        ProxyPassReverse /demo http://sqlv01:51001/demo
        SSLEngine On
        SSLCertificateFile C:\Apache_server\ssl_certs\STAR_spidexsaas_co_uk.crt
        SSLCertificateKeyFile C:\Apache_server\ssl_certs\privatekey.key
        SSLCertificateChainFile  C:\Apache_server\ssl_certs\STAR_spidexsaas_co_uk.ca-bundle
       
        Redirect / http://www.spidexsoftware.co.uk/
&lt;/VirtualHost&gt;

&lt;VirtualHost *:443&gt;
        ServerName wm.spidexsaas.co.uk
        ProxyRequests Off
        ProxyPreserveHost On
       
        &lt;Proxy *&gt;
                Order deny,allow
                Allow from all
        &lt;/Proxy&gt;
        ProxyPass /demo http://sqlv01:51002/demo
        ProxyPassReverse /demo http://sqlv01:51002/demo
        ProxyPass /demo-admin http://sqlv01:51002/demo-admin
        ProxyPassReverse /demo-admin http://sqlv01:51002/demo-admin
               
        SSLEngine On
        SSLCertificateFile C:\Apache_server\ssl_certs\STAR_spidexsaas_co_uk.crt
        SSLCertificateKeyFile C:\Apache_server\ssl_certs\privatekey.key
        SSLCertificateChainFile  C:\Apache_server\ssl_certs\STAR_spidexsaas_co_uk.ca-bundle
       
        Redirect / http://www.spidexsoftware.co.uk/
&lt;/VirtualHost&gt;
</pre>

Research Update 02/04/2018

Hiding Headers  
https://techglimpse.com/set-modify-response-headers-http-tip/
https://scotthelme.co.uk/hardening-your-http-response-headers/

Getting an A+ on SSLLAbs  
https://itigloo.com/2017/02/21/how-to-get-an-a-rating-with-100-score-on-the-ssllabs-test-with-apache/
https://httpd.apache.org/docs/trunk/ssl/ssl_howto.html#onlystrong
https://sevenminuteserver.com/post/2017-05-04-getana+qualysssl/




