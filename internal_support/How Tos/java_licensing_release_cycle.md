New Oracle Java Release Cadence  
https://blogs.oracle.com/java-platform-group/update-and-faq-on-the-java-se-release-cadence

Java SE Subscription
https://www.oracle.com/technetwork/java/javaseproducts/overview/index.html

Java SE Subscription FAQ
https://www.oracle.com/technetwork/java/javaseproducts/overview/javasesubscriptionfaq-4891443.html

Microsoft and Azul Zulu  
https://www.azul.com/press_release/free-java-production-support-for-microsoft-azure-azure-stack/
https://azure.microsoft.com/en-us/blog/microsoft-and-azul-systems-bring-free-java-lts-support-to-azure/  
Zulu free enterprise support for production use 🙂







