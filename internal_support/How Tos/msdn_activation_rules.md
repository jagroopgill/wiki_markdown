# MSDN Activation Rules

## Product Keys and Activation
Microsoft is committed to protecting the integrity of the Visual Studio subscription program and the value of the software and benefits that the program’s legitimate subscribers receive for their investment. In support of this commitment, Microsoft implemented the following changes on July 16, 2012:

- <b> Windows & Office product key reductions: </b>        subscriptions include five product keys for current versions of Windows and Office products and three product keys for older versions (previously, there were a varying range of keys provided for each product).
- <b> Daily product key claim limit reduction:</b> the number of product keys that can be claimed within a 24-hour period is set to ten for all Visual Studio subscription levels, including those provided through MPN. Exception: a daily claim limit of seven product keys for Visual Studio Professional (MPN)..
- <b>Reduction of redundant product keys: </b>  Select Office suites and standalone Office product keys are no longer available in subscriptions that include Office. Product keys are still available for the corresponding highest Office suite of the same version (e.g., OneNote 2010 will be removed and will be available in Office Professional Plus 2010). Additionally, previously claimed keys continue to be available.

<b>Please note that each product key has multiple activations and many products allow you to install and use the product without activation for 30 days or more.</b> Our research on historical key claiming patterns shows that the majority of subscribers are not impacted by these changes. However, if activating a product key multiple times does not meet your needs or you’ve exhausted the activations, you can submit a request for additional keys through Visual Studio Subscription Customer Service.

There are no changes to Visual Studio licensing at this time (including perpetual usage of software). Learn more about Visual Studio licensing.

Visual Studio subscriptions provide access to a wide range of Microsoft products for designing, developing, testing, and demonstrating applications. Some of these products require product keys during installation, and some of those require activation.

Visual Studio subscriptions typically include five product keys for current versions of Windows and Office products, and three keys for older versions.

Product keys and product activation can be a tricky topic, so we’ve assembled some useful links to better understand and be better prepared to plan out your development and testing environments as a result.

### <b>Resources for Product Keys and Activation:</b>

-Windows Volume Activation
-Windows Activation in Development and Test Environments
-Product activation and key information
-Frequently asked questions about Volume License keys
-Volume Activation Management Tool 3.1: This tool has a number of features, including the ability to determine how many times your MAK keys have been activated. VAMT 3.1 can be installed as part of the Windows Assessment and Deployment Kit (Windows ADK). Learn more about installing the Windows ADK.

## Activating a Product
Activation is required for certain products; generally in Visual Studio subscriptions this is limited to Windows, Windows Server, Microsoft Office client applications, and Expression Studio. When activation is required, many products—including Windows and Office—allow you to install and use the product without activation for 30 days or more. If you re-image your computers frequently (at least once every 30 days), consider NOT activating the product. Product keys supplied as part of your Visual Studio subscription do not allow unlimited activations of a product.

If you’ve claimed all the available product keys under your subscription and have used up all the available activations, you can request additional keys by contacting your local Visual Studio Subscription Customer Service Center. Requests will be considered on a case-by-case basis. If approved, product keys will be accessible in Subscriber Downloads.

## Retail and Volume Licensing Versions of Products

Visual Studio subscriptions offer a selection of retail and Volume Licensing versions of Microsoft products. Certain products are available from Subscriber Downloads only in their retail version or only in their Volume Licensing version; some are available in both versions. Generally, products requiring activation, such as Windows, Windows Server, and Office client products, are only offered in their retail version through Subscriber Downloads.

Volume Licensing customers can also download software from the Volume Licensing Service Center (VSLC) to get Volume Licensing versions of products (and the corresponding product keys).  Access to the VLSC is managed by designated administrators within the customer’s organization.

## Locating and Claiming Product Keys

You must be signed in with the Microsoft account associated with your Visual Studio subscription to view your product keys. Individual product keys are found by selecting the Keys button for a particular product on the Subscriber Downloads page. All keys are also available in aggregate on the My Product Keys page. When multiple keys exist for a single product, Key Notes will be displayed on the Details column for the download to assist you in identifying which key should be used.

Some products bundle multiple editions of the product into a single download. In these cases, the product key entered determines which edition of the product is installed.

Only subscribers with active subscriptions can download products and claim product keys, so you will not be able to download products after your subscription has expired. In some cases, expired subscriptions can still display keys claimed while the subscription was active, but expired subscriptions are unable to claim new keys. To access previously claimed keys from an expired subscription, you must be signed in using the Microsoft account used with that subscription.

Some keys are provided automatically, such as “static” keys, which you can use as many times as needed because activation is not required. Other keys must be claimed by selecting the Get Key or Get Another Key button for the product.

### <b>Product Key Types Listed in Subscriber Downloads</b>

Key Type |	Description
---|---
Not Applicable	| No key is needed to install this product.
Retail	| Retail keys allow multiple activations and are used for retail builds of the product. In many cases, 10 activations are allowed per key, though often more are allowed on the same machine.
Multiple Activation	|A Multiple Activation Key (MAK) enables you to activate multiple installations of a product with the same key. MAKs are generally used with Volume Licensing versions of products. Typically, only one MAK key is provided per subscription.
Static Activation Key	| Static activation keys are provided for products that do not require activation. They can be used for any number of installations.
Custom Key |	Custom keys provide special actions or information to activate or install the product.
VA 1.0	| These are multiple activation keys, similar to a MAK.
OEM Key |	These are Original Equipment Manufacturer keys that allow multiple activations.
DreamSpark Retail Key |	These retail keys are for DreamSpark and allow one activation. DreamSpark Retail keys are issued in batches and are primarily intended for student consumption.
DreamSpark Lab Key |	These lab use keys are for DreamSpark programs and allow multiple activations. DreamSpark Lab Keys are intended for use in university computer lab scenarios.
DreamSpark MAK Key |	These are MAK keys for DreamSpark program customers.
## Daily Product Key Claim Limits
Microsoft is committed to protecting the integrity of the Visual Studio subscriptions program and the value of the software and benefits that the program’s legitimate subscribers receive for their investment. In support of this commitment, Microsoft has implemented daily key claim limits for Visual Studio subscriptions. Daily key claim limits restrict the number of keys that can be claimed per day per subscription.

The daily key claim limit is 10 keys for all Visual Studio subscriptions, including those offered through MPN, BizSpark, MCT Software & Services, and MCT, with the exception of Visual Studio Professional (MPN), which has a daily key claim limit of 7 keys.

Note: key claim limits are subject to change. Please check here for the most up-to-date limits.

If you have claimed the maximum number of keys available with for your subscription within a 24-hour period, you will see an error telling you that your limit has been reached. The limit resets every 24 hours, starting at 12:01 AM PST.

Our research shows that the majority of subscribers are covered by the number of product keys included in their subscription. If you need additional keys, you can submit a request through Visual Studio Subscription Customer Service and it will be considered for approval on a case-by-case basis.

## Exporting and Saving your Product Keys
You can export and save all your product keys from the My Product Keys page so that you don’t need to be online to access them.  You can open the XML key list using XML Notepad, Microsoft Excel, or other XML compatible programs. XML Notepad 2007 provides a simple intuitive user interface for browsing and editing XML documents, and can be downloaded from the XML Notepad 2007 page.

To open the Product Key Export using Microsoft Excel:

1. Log into Subscriber Downloads and go to the Product Keys List page.
2. Click the Export Key List to XML link.
3. Save the file to your hard disk.
4. Start Excel.  On the File menu, click Open and browse to the file you saved in step 3.
5. If a dialog box appears displaying a message about a schema not being referenced, click the OK button. You can also check the box to not see this message in the future.

## Internet Demonstrations via Terminal Services
With a Visual Studio subscription, you are allowed to provide end users access to Internet demonstrations of your programs via Terminal Services (Windows Server 2003 or Windows Server 2008) or Remote Desktop Services (Windows Server 2008 R2 and later). Up to 200 anonymous users can simultaneously access your demonstration this way. Your demonstration must not use production data. Visual Studio subscribers are licensed to demonstrate their applications to end users. This Internet demonstration using Terminal Services (TS) or Remote Desktop Services (RDS) is the only scenario where end users without a Visual Studio subscription can interact with the demonstration application when the software is licensed through Visual Studio subscriptions.

This is in addition to dev/test rights, where Visual Studio subscribers can use as many RDS or TS connections as needed.

### Enabling RDS Access

Visual Studio subscribers can increase the number of users who can access a Windows Server via RDS by entering a product key supplied in the My Product Keys tab on the Subscriber portal. To obtain a product key, connect to the My Product Keys page and scroll down to the version of Windows Server you are running. Locate “Windows Server <version> R2 Remote Desktop Services <user or device> connections” and click the “Get a Key” button. For example, if you’re using RDS on Windows Server 2012 R2 and your deployment uses user CALs, choose “Windows Server 2012 Remote Desktop Services user connections (50)”.

Five keys of each type are available for Windows Server 2008 R2, and each key will support 20 connections. For Windows Server 2012 R2, four keys for each type are provide and will support 50 connections each. 

To enable additional connections in Windows Server: 

1. Open Server Manager.
2. Open the Servers list in the left nav pane.
3. Right click on your license server and choose “Install Licenses”.
4. Follow the steps in the wizard.  When you’re selecting the agreement type, choose “License Pack (retail)” and enter the product key you obtained from the Subscriiber portal. 

End users can connect to access applications via RDS as long as the following conditions are met: 

- Users must be anonymous (in a non-authenticated state).
- Connections must be over the Internet. 
- Up to 200 concurrent user connections may be used for demonstrations of the application. 
- The product keys to enable user connections must be obtained by a Visual Studio subscriber.

If you need instructions for setting up RD Licensing on your server, please see RD Licensing Configuration on Windows Server 2012. If you have any questions, please visit the Microsoft Remote Desktop Services forum. 