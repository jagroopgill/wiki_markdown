# Ora12c Notes

Install as normal

Create a windows User

spx-ora12c-user
doesn'tneedadecentpassword1d

install to c:\app\oracle

install a listerner (orcllsnr)

choose advanced mode to create the db

create a container database as empty

admin password
adminYeahSp1dexsa1234!

pluggable db = basedb

EM Database Express Port 5500

Use Common Location For files

Once base scripts done (with no pluggable) then backup the db
alter database archivelogmode
rman target /
backup database;
Create a pluggable (database creation assistant)
administrator  ideal_orig_admin
password adminYeahSp1dexsa1234!
datapump C:\app\oracle\oradata\datapump\daa_original  (daa_orig_pump)
conn username/password@spx-ora12c:1521/daa_original
OR TNSNAMES
conn username/password@daa_original
If the connection fails with archivelog error and talking about space issues
Oracle has a fixed area size called FAST_RECOVERY_AREA, which has a startup parameter called  DB_RECOVERY_FILE_DEST_SIZE

The rest of this you can only do if this is erroring by logging as conn / as sysdba on the server as Oracle will block all other connections from remote users

You can check what this is by running...

SQL> SELECT * FROM V$RECOVERY_FILE_DEST;

NAME
--------------------------------------------------------------------------------

SPACE_LIMIT SPACE_USED SPACE_RECLAIMABLE NUMBER_OF_FILES     CON_ID
----------- ---------- ----------------- --------------- ----------
C:\app\oracle\fast_recovery_area
1.6106E+10 1.0696E+10        1765782016              58          0

So here, we have 1765782016/(1024*1024) = 1.6GB free

FAST_RECOVERY_AREA is filled up by RMAN - Short for recovery manager
What we need to do is take a backup and then delete all the old archive files.  We can only do this in RMAN, so from command line:-

rman TARGET /

backup as backupset database plus archivelog;

If you get an error then probably need to increate the recovery area - be careful, note how much disk space is left first!  


sqlplus / nolog
conn / as sysdba

Then retry the backup as backupset command above

If this is OK , we can now delete all the old arhivelog files and backups we don't need, back to 7 days of consistancy

crosscheck backup;
crosscheck archivelog all;
delete noprompt expired backup;
delete noprompt expired archivelog all;
report obsolete recovery window of 7 days;
delete noprompt obsolete recovery window of 7 days;
backup as compressed backupset archivelog all;
delete noprompt archivelog until time 'sysdate-1' backed up 1 times to device type disk;

SHould be ok to login as whomever now.

https://dba.stackexchange.com/questions/119310/archive-logs-filling-up-flash-recovery-area-fra-space

This whole mess creates a large amount of log files in...

C:\app\oracle\diag\rdbms\orcl12c\orcl12c\trace

Can delete pretty much everything from here APART FROM ALERT_ORCL12C.LOG
:)