# Sys Internals Tools

### Proc Mon
This shows historically what the system has been doing, so you run it, let it capture what was going on and then analyze.  
It shows all types of reads/writes, files, dlls  
Use filter to filter down to a specific executable to see exactly what it has done over the period of the capture

### Proc Exp
http://www.howtogeek.com/school/sysinternals-pro/lesson2/  
Shows what is going on right now  
Have to run as administrator and also best to setup access to the online symbols   - http://www.hanselman.com/blog/SetUpYourSystemToUseMicrosoftsPublicSymbolServer.aspx    
Then to see what handles a process is using it's Ctrl + H  
To see what dll's it's Ctrl + D  
To get the stacks double click on the process, then on the thread  
This is what we used to look at hosted datasabre
