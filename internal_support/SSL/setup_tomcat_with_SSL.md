# Setup Tomcat with SSL

## Create the Keystore
Login to the server where you will be setting up SSL.

1. Enter the following into a command prompt $JAVA_HOME/bin/keytool -genkey -alias 'Enter Alias ie Servername' -keyalg RSA -keystore 'path to TomcatHome\ssl-keystore'  
2. Take note of the alias as you will need this later
3. Enter the following details the customer may provide this information:
- keystore password: 'Enter a password and take note'
- First and Last Name —
- Organizational Unit —
- Organization —cls
- 
- City/Locality —
- State/Province —
- Country Code —
- Enter password again  

This will create your keystore

## Create the CSR file

- Enter the following command $JAVA_HOME/bin/keytool -certreq -keyalg RSA -alias 'Enter the Alias from your last command' -file 'use alias'.csr -keystore 'path to keystore'
- This creates the csr file

Send the csr file to the customer and ask for the certificate and root certificate in return.

## Importing the Certficates 

Open a command prompt in order to install both certificates

- Run the following to import the Root Certificate
keytool -import -alias root -keystore 'path to keystore' -trustcacerts -file 'path to root certificate'

- Run the next command
keytool -import -alias 'Enter your alias' -keystore 'path to keystore' -file 'path to certificate'

## Configuring Tomcat to use SSL
- Edit your server.xml C:\TOMCAT_HOME\conf\server.xml and locate 8443 and replace with the following

&lt;Connector port="8443" maxThreads="400" scheme="https" secure="true" SSLEnabled="true" keystoreFile="Z:\apache_tomcat_isis\ssl-keystore" keystorePass="55lSp1d3x15IS^" clientAuth="false" keyAlias="test-isis" sslProtocol="TLS"/&gt;

- Locate and comment out the below command

<--APR library loader. Documentation at /docs/apr.html>

- Edit your web.xml C:\Tomcat\WM\apache-tomcat-7.0.50\webapps\wmm-webclient\WEB-INF\web.xml and add the following

&lt;security-constraint&gt; &lt;web-resource-collection&gt; &lt;web-resource-name&gt;Entire Application&lt;/web-resource-name&gt; &lt;url-pattern&gt;/*&lt;/url-pattern> &lt;/web-resource-collection&gt; &lt;user-data-constraint> &lt;transport-guarantee>CONFIDENTIAL&lt;/transport-guarantee&gt; &lt;/user-data-constraint&gt; &lt;/security-constraint&gt;  
http://www.mulesoft.com/tcat/tomcat-ssl

### Configuring Tomcat to use SSL using P7B cerficate

https://www.digicert.com/ssl-certificate-installation-tomcat.htm
https://knowledge.rapidssl.com/support/ssl-certificate-support/index?page=content&actp=CROSSLINK&id=SO16181

## Renew Certificate
In order to renew the certificate:

Copy the new p7b over the old one

Open command prompt as admin

Change directory to C:\Program Files\Java\jdk1.8.0_51\bin

Run keytool -import -trustcacerts -alias CLEVELAND -file "C:\Program Files\Spidex\cert\mycertificate.p7b" -keystore "C:\Program Files\Spidex\cert\keystore\SSL"

Then enter this password 21h*0H044Y

Stop Tomcat  
Delete the contents of Tomcat\Home\work\Catalina\localhost  
Delete the content of temp  
Start Tomcat
