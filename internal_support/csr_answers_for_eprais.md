# CSR Answers for www.eprais.com

nstevens@spx-bntkgh2:/mnt/c/dump/ssl_wwwepraiscom$ openssl req -new -key www.eprais.com.key -out www.eprais.com.csr  
You are about to be asked to enter information that will be incorporated  
into your certificate request.  
What you are about to enter is what is called a Distinguished Name or a DN.  
There are quite a few fields but you can leave some blank  
For some fields there will be a default value,  
If you enter '.', the field will be left blank.  
-----
Country Name (2 letter code) [AU]:GB  
State or Province Name (full name) [Some-State]:Warwickshire  
Locality Name (eg, city) []:Coleshill  
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Spidex Software Ltd  
Organizational Unit Name (eg, section) []:marketing  
Common Name (e.g. server FQDN or YOUR name) []:www.eprais.com  
Email Address []:  
Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:  
An optional company name []:  


