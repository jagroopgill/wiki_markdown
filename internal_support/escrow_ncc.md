# Escrow/NCC

THis note contains how to deposit new code.  We can only do this once a year otherwise we are charged.

## EscrowLive Activation

Welcome to Escrow Live, as a valued client you have already have been granted access to our secure web portal. To access this service please go to https://www.escrowlive.net/Login.aspx, where you can log in using your e-mail address and the password below:

Login: netsupport@spidex.co.uk

Password: isbs3983IS 

Our secure web portal allows our global escrow client base to manage their accounts online. Registered clients can currently:
- view existing agreements
- request to change contacts on any agreement
- register additional licensees to a multi-licensee agreement (suppliers)
- deposit source code securely for each agreement (Feature not available to Licensees)
- view any existing deposits and the Integrity Testing Reports

### Security 

Security and performance were of paramount importance when developing the portal due to the size of the files which are uploaded and the huge commercial implications of unauthorised access to proprietary source code. We drew on the extensive experience of our highly qualified and experienced security and performance testing teams who were closely involved in the specification, development and rigorous testing of the portal.

To upload a deposit-
- Select the agreements tab
- Select the relevant agreement by clicking on the agreement number
- Select 'Deposit'
- You will then be forwarded to a Source Code Deposit Form which needs to be completed, please do this and click 'Submit' at the bottom of the page. The form will then be processed.
- A new page will load, which advises that your deposit has been successfully prepared
- Click on the 'Deposit' button which will load the upload window
- Select the files you want to upload, ensuring that they make a complete deposit and click the 'Upload' button
- When the upload has finished, a confirmation window will appear to confirm that your upload was successful.

<u>Please note that each successful upload is treated as one deposit. If you intend to make multiple uploads against the same agreement number, please notify us accordingly otherwise they will be processed separately.</u>

Recent deposits will only show as ‘Deposited’ once it has been processed by our administration department and this can take up to 72 hours, especially during busy periods. 

<span style="color:red">If you require technical assistance from the Integrity Department our support hours are Monday to Friday 9am to 5:30pm UTC.  
Our contact details are shown below 0161 209 5209 or via e-mail at Integrity@nccgroup.com </span>

Thank you
NCC Group
  