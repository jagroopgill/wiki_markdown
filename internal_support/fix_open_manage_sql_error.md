# Fix Open Manage SQL Error

Sometimes when dc1 has been rebooted Open Manage cannot connect to it's db after, this is because for some reason the SQL services stop.

RDP to spx-dirsynch and check the services that are marked are running, if they are not start them and try and login again.

![Alt Text Here](19b99614-66bd-4127-b8c7-b0067095d066.PNG)  

Here's an example error message if it's not running:-

DataCache.LoadCheck()                Exception loading the cache occurred: System.ServiceModel.FaultException: <?xml version="1.0" encoding="utf-16"?><ArrayOfKeyValueOfstringstring xmlns:i=" http://www.w3.org/2001/XMLSchema-instance " xmlns="http://schemas.microsoft.com/2003/10/Serialization/Arrays"><KeyValueOfstringstring><Key>Message</Key><Value>A network-related or instance-specific error occurred while establishing a connection to SQL Server. The server was not found or was not accessible. Verify that the instance name is correct and that SQL Server is configured to allow remote connections. (provider: TCP Provider, error: 0 - No connection could be made because the target machine actively refused it.)
#10061.0</Value></KeyValueOfstringstring><KeyValueOfstringstring><Key>Details</Key><Value>System.Data.SqlClient.SqlException (0x80131904): A network-related or instance-specific error occurred while establishing a connection to SQL Server. The server was not found or was not accessible. Verify that the instance name is correct and that SQL Server is configured to allow remote connections. (provider: TCP Provider, error: 0 - No connection could be made because the target machine actively refused it.) ---&gt; System.ComponentModel.Win32Exception (0x80004005): No connection could be made because the target machine actively refused it
   at System.Data.ProviderBase.DbConnectionPool.TryGetConnection(DbConnection owningObject, UInt32 waitForMultipleObjectsTimeout, Boolean allowCreate, Boolean onlyOneCheckConnection, DbConnectionOptions userOptions, DbConnectionInternal&amp; connection)