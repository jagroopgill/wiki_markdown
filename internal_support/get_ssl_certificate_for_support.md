# Get an SSL Certificate for support.spidex.co.uk

1. Might have to buy one from godaddy, there are often deals if you login using the note (Login for godaddy SSL), got one for £3.99+Vat for support.spidex.co.uk
2. Generate a csr, so go to the apache into the bin directory in admin level dos
3. You need to point the OPENSSL exe to the correct config file, for the tomcat I used on spx-web2 this was:-
          set OPENSSL_CONF=C:\apaches\apache-proxy\conf\openss l.cnf
4. Now generate the request
          C:\apaches\apache-proxy\bin>openssl req -new -newkey rsa:2048 -nodes -keyout sup port.spidex.co.uk.key -out support.spidex.co.uk.csr
5. Paste that into godaddy CSR generation and make the request
6. Before doing this I had created a user called admin@spidex.co.uk in office 365 and granted them outlook access.  Their password is 1nternalSupp0rt
7. Login as them and grant the domain access request
8. Then you wait a little while and get a download button, choose download and get the zip file for apache.
9. Can now setup SSL for apache as normal