# How to code the alarm fobs

- enter the alarm code in and press enter  
- type 42 and press enter twice  
-- should now see something with a square and an m in the bottom right corner  
- scroll through the users pressing A-> until find a square on it's own without an m  
- press enter  
- scroll through the options using A-> until 6=prox tag press enter  
- screen now looks like  
<pre>
Prox tag
>
</pre>

- Press 1 and A at the same time briefly, a cursor now appears next to the greater than
- hold the fob bottom right until it bleeps and 9 characters appear  

Press Enter

- Click A-> until 8=Max function appears, press enter
- Type 12, which is "set and unset" press enter
- Escape all the way out
- Check the fob