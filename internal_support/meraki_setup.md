# Meraki Setup

http://meraki.cisco.com  
Login to get to the dashboard to configure the device  

Spidex Office (Hidden)  
     WPA Key - G1v3Th1sOutP45^  
     Has LAN Access and unlimited Bandwidth  
     *Do not tell anyone this password - we must enter it!  

Spidex Internet  
     WPA Key - 3mployee1nternet  
     No LAN Access and 5MB/s Bandwidth  
     Extra filtering  

Spidex Guest  
     WPA Key - SpidexWelcomesYou  
     No LAN Access and 2MB/s Bandwidth  
     Extra filtering  

Spidex 500Ks  
    WPA Key - SlowAsSnails  
    No LAN Access and 500Ks Bandwidth  
    No Adult content, everything else is allowed  

The 10.0.1.10 I.P. Address is passed through a TCP and UDP Proxy on the firewall which has the "marketing" HTTP filteing proxy.  SIP and FTP are disallowed

Splash Page Login for "Spidex Internet"  
internet@spidex.co.uk  
internet  
