# Fixing AD Synchronisation Error from Microsoft

If we get these emails:  
Hello nicole.stevens@spidex.co.uk,  

On Tuesday, 20 October 2015 13:42:17 GMT, Azure Active Directory did not register a synchronization attempt from the Identity synchronization tool in the last 24 hours for Spidex Software Limited [spidexsoftware.onmicrosoft.com].

You can troubleshoot this issue by running the Directory Synchronization troubleshooter on the server that has Azure Active Directory identity synchronization tools installed.

Thank you,

The Azure Active Directory Team

Do not reply to this message. It was sent from an unmonitored email account.

We need to do this on spx-dirsynch  (elevated dos/powershell)

In order to run the “start-onlinecoexistencesync” between Office 365 and on Premises AD you will need to open PowerShell in Admin mode then import-module Dirsync.  Once imported you will be able to execute the synchronization.

so

1.  Open an admin level powershell
2.  Type import-module Dirsync
3.  Type start-onlinecoexistencesync

You might need to set the remote policy for powershell but that should be it.