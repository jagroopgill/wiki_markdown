# How to get the office working if the DC goes down

Create static addresses for all of the workstations
- At the time of writing the ip rage was on 10.0.1.x subnet 255.255.255.0
- So change to addresses on the 100 range and hardcoded

IMAGE

- This also shows setting up the alternate DNS settings to opendns - you'd need to check the current opendns i.p address when you add this, this allows extrernal web addresses to resolve  
- 10.0.1.253 is the gateway which is the i.p. address of the firewall

### Add spx-web and 10.0.1.2 to the hosts file

- This is specificatlly to get spx-web to resolve to 10.0.1.2, this allows subversion to work and jira internally
- If subversion / jira have moved to a different box or spx-web's i.p. address has changed then adjust accordingly

### Remove the support.spidex.co.uk alias from jira conf

- This allows jira to be resolved internally to spx-web rather than support.spidex.co.uk which will not work with internal DNS down

<span style="color:red"> &lt;Alias&gt;support.spidex.co.uk &lt;/Alias&gt; </span>   At the bottom of the file delete this line

-     <Connector port="8088" maxHttpHeaderSize="8192"
               maxThreads="150" minSpareThreads="25" maxSpareThreads="75"
               enableLookups="false" redirectPort="8443" acceptCount="100"
               connectionTimeout="20000" disableUploadTimeout="true" URIEncoding="UTF-8"
                  proxyName="support.spidex.co.uk" proxyPort="80"/>         Delete the bits in red
- Then restart Jira  

### Removed security from watchguard - the backup for the watchguard setup is on spx-web on my desktop

- I'll be honest here, I just kept hackign at the authentication until it worked but basically I...
- Backed up the firebox and stored the file on spx-web
- disbaled all but the domain HTTP proxy
- deleted the authentication users and groups for AD and turned off authentication on domain

### Removed the subversion AD config from code and docs and restarted

- on spx-web stopped svn_docs apache service (this also stops the wiki's too)
- backup \conf\httpd.conf for the svn_docs apache
- Edit the file, at the bottom is the declaration for svn in this section - <Location /svn>
- Comment out the authentication section
 -    AuthName "Subversion Authentication"
     #AuthType SSPI
     #SSPIAuth On
     #SSPIAuthoritative On
     # set the domain to authorize against
     #SSPIDomain SPIDEX
     #SSPIOmitDomain On      # keep domain name in userid string
     #SSPIOfferBasic On      # let non-IE clients authenticate
     #SSPIBasicPreferred Off # should basic authentication have higher priority
     #SSPIUsernameCase lower

     # require the SVN Users group
     #Require group "SPIDEX\Development"
- Restart subversion and the authentication has gone so code can be checked in - note this means no username is stored in the log, but there's not much we can do in a rush about this!
- Do the same for the code repository which is in e:\apache_vtiger
