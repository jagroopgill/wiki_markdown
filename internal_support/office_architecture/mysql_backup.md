# MySQL Backup

Locate mysql backup all.bat on spx-web on E:

Find the lines underneath ':: Run mysqldump on each database and compress the data by piping through gZip' - this will give you the database backup line

Copy
<pre>
"%mysqldir%\bin\mysqldump" --user=%dbuser% --password=%dbpass% --databases %%f --opt --quote-names --allow-keywords --complete-insert > "%bkupdir%\%fn%\%fn%_%%f.sql"
</pre>

Copy all required parameters apart from folders and file names which are hard coded. hard code the database name.

Example:
<pre>
:: Install folder of MySQL
set mysqldir=C:\Program Files\MySQL\MySQL Server 5.5

:: Name of the database user
set dbuser=nicole_admin

:: Password for the database user
set dbpass=nicole_admin

:: directory where backup files are stored
set bkupdir=e:\backup_tools_scripts_logs\backups

"%mysqldir%\bin\mysqldump" --user=%dbuser% --password=%dbpass% --databases jira_5_live --opt --quote-names --allow-keywords --complete-insert > "%bkupdir%\jira5livebak.sql"
</pre>

To Restore:

Edit SQL file remove create database statement and set the use parameter to the database you are restoring to.

Run SQL File in mysql workbench
