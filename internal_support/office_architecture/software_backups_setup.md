# Software Backups Setup

## SlikSVN
On Hyperv2 the sliksvn client is installed -https://sliksvn.com/download/  
A script running on hyperv2 in C:\file_backups\scripts\sliksvn_dump_all is executed by windows task scheduler every evening at 7PM  
The script uses the sliksvn client to dump each repository to C:\file_backups\temporary_data_folders\sliksvn  
Then zips it to C:\file_backups\backed_up_data\sliksvn which is cleared down first by the backup script  
Cloudbacko then backs up the files in C:\file_backups\data\sliksvn to azure and local USB overnight


## Jira Repo
A script running on hyperv2 in C:\file_backups\scripts\jira_data is executed by windows task scheduler every evening at 7.30PM  
The script is a simple call to a zip client to zip all the jira data on spx-web2 to C:\file_backups\temporary_data_folders\jira_data  
Then zips it to C:\file_backups\backed_up_data\jira_data which is cleared down first by the backup script  
Cloudbacko then backs up the files in C:\file_backups\data\jira_data to azure and local USB overnight


## MySQL
A script running on web2 in C:\mysql backups\scheduled_script is executed by windows task scheduler every evening and lunchtime at 7PM and 12PM  
the script exports all the dbs present on web2 to C:\mysql backups\scheduled_script_dumps, which is cleared down first by the backup script  
The script then zips up all the files in this directory and copied the zip drive to \\spx-hyperv2\mysql$ (which is cleared to last 30 days backups)  
Cloudbacko then backs up the files in C:\file_backups\data\jira_data to azure and local USB overnight


## PCloud
Scheduled tasks run each evening to backup base folders on PCloud  
Sunday - professional services  
Monday - development  
Tuesday - sales  
Wednesday - marketing  
Thursday - Accounts, QA, Helpdesk, Technical  
Friday - Releases  
The task activates a PDI interface which calls the PCloud API to create a backup file of the given folder and then download this zip file.  If the base folder is larger than 2GB, the subdirectories of the base folder are zipped and downloaded one by one.  
Cloudbacko then backs up the files in C:\file_backups\data\pcloud to azure and local USB overnight.  
One weeks worth of backups are kept as this is just incase PCloud goes down