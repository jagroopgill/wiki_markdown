# Upgrade to Jira Software

Note – War distribution is no longer supported. We have now moved to the standalone version.

Shutdown Jira

Backup jira home

backup database

backup tomcat directory

Shutdown the proxy apache server

create new jira database

create a new jira user
run grant privileges script
restore the backup to the new database (rename sql file with correct database)

Copy in Jira standalone tomcat

edit bin\setenv file

point jira_home to your jira home in atlassian-jira\WEB-INF\classes\jira.application.properties
create dbconfig xml in new jira home directory, update the URL to latin1, and the username/password/host settings for the new db and user

edit dbconfig.xml change storage_engine to default_storage_engine
delete the lock file from jira home directory

update the server.xml properties and change the ports to those that are not in use by the other tomcat and apache installations

check the show variables like 'innodb_lock_wait_timeout'; command on SQL make sure it's still 150

start tomcat, you might get some errors,

this will update your new database to the latest version

stop it when the server startup happens

copy the mysql jar into webapps\jira\lib

edit the atlassian-jira\WEB-INF\jira.application.properties and add the new jira home in

edit the atlassian-jira\WEB-INF\classes\templates\plugin\comment\system-comment-field-edit.vm

convert this section

#if ($commentLevel)
    #set ($selectedCommentLevel = $commentLevel)
#else
    #set ($selectedCommentLevel = "")
#end
to

#set ($selectedCommentLevel = "")
#if ($jiraUserUtils.getGroupNamesForUser($authcontext.loggedInUser.name).contains('jira-developers'))
  #set ($selectedCommentLevel = "role:10001")
#end
Startup - hopefully that should be it! Note : you'll need to use spx-web:xxx/jira because of the proxy setup

Add the new Jira service

Configure the proxy settings changes into the server.xml (see old tomcat installation)


Specific Tests Security Field - ensure it is updated with the correct value
Emails are sent out to reporter on comment, update of the issue etc

Can we configure so that the default group is "developers" when a comment is added - might need Nicole to make code changes for this?

Do the linked drop downs work in the Mainsaver Project?

Check informaiton on the two plugins we had in Jira 3/4 :-

1) Set Issue Security Level Plugin - This plugin automatically sets an issues' security level based on a user's specified group

2) ILOG Security Level Searcher plugin - ILOG Security Level Searcher plugin allows to search and filter on security levels

Check into the errors that come up when you look at administration


Changes after install
1. Disable the hipchat plugin.  Administration -> Add Ons.  manage Add ons.  Click disable on HipChat Plugin.

