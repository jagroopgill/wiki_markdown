# Setup Server Time Sync

The time for workstations throughout the office is synched from spidex-dc1.

This page explains how the time sync is setup to work. All commands on this page are to be run in an elevated dos prompt.


Check the current setting
<pre>
w32tm /query /status
</pre>
This will display the current settings of the time synch
<pre>
C:\Users\nstevens>w32tm /query /status
Leap Indicator: 0(no warning)
Stratum: 3 (secondary reference - syncd by (S)NTP)
Precision: -6 (15.625ms per tick)
Root Delay: 0.2128844s
Root Dispersion: 0.5859611s
ReferenceId: 0x41371513 (source IP:  65.55.21.19)
Last Successful Sync Time: 02/11/2011 12:07:12
Source: time.windows.com,0x1
Poll Interval: 6 (64s)

w32tm /config /manualpeerlist:pool.ntp.org
</pre>

To set the server time to the correct UK server pool giving
<pre>
C:\Users\nstevens>w32tm /query /status
Leap Indicator: 0(no warning)
Stratum: 3 (secondary reference - syncd by (S)NTP)
Precision: -6 (15.625ms per tick)
Root Delay: 0.0570047s
Root Dispersion: 1.7905033s
ReferenceId: 0x5F597C59 (source IP:  95.89.124.89)
Last Successful Sync Time: 02/11/2011 12:21:17
Source: pool.ntp.org
Poll Interval: 6 (64s)
</pre>
Note the part that has changed is the source.

Further Information