# Snowdrop Support

From: Miriam González   [mailto:miriam@snowdropsolutions.co.uk]  
Sent: 18 November 2016 10:22  
To: Nicole Stevens <nicole.stevens@spidex.co.uk>  
Subject: Welcome to Google Maps via Snowdrop  
Hello Nicole,  
We're very excited that you have chosen to continue to partner with Snowdrop Solutions Ltd.  
By now, your technical contact has already received your renew mail from Google Maps, and the Premium Plan Maps API licenses and support are available and operational. This email is to help make sure that you and your team have access to the Google Maps API licenses as well as the key services and contacts as outlined below.  
Here are the key resources to get you started and provide help where necessary:  

  _ |   Details
 ---|---
Google SKU’s purchased:|  MA4WOEM500K  
Licenses End Date:| 15 Nov 2017  
Project ID:  |Project ID: 640069075008  <br>Project URL: https://console.developers.google.com/project/640069075008  <br>Google Account (to access the project):   spidexisis@gmail.com  
Client ID:  |Client ID: gme-spidexsoftwareltd  <br>Your Private Crypto Key (for use with your Client ID): PQbPvgnX4Kmw9UPeykjnfvZsvNs=  
<b>Area </b> |<b> How to contact  </b>
Account management:  Request information from the Snowdrop account team  | info@snowdropsolutions.co.uk  
News and updates from Snowdrop:  You have been added to our subscriber list. To opt-out, send email to with UNSUBSCRIBE to the address at right |news@snowdropsolutions.co.uk  
Invoicing / Financial: Questions about invoices, payment and related terms |finance@snowdropsolutions.co.uk
Please do feel free to make use of the above resources.
We will contact you again within three months of your license start date to discuss your usage of Google Maps; and we’ll contact you again in around nine month’s time to review your projected usage and help to identify your requirements for the following year.
Thank you again for choosing Snowdrop and we look forward to working closely with you!
-Onboarding Wizard
Miriam González
Snowdrop Solutions Ltd.
…………………………………………………………...

