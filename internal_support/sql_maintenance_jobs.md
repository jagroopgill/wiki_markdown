# SQL Maintenance Jobs

## Setup of Maintenance Plans
Instead of Maintenance plans we use SQL Server Maintenance Solution or maintenancesolution.sql This is a free package of SQL tools allowing a DBA more flexibility around maintenance procedures.

### Install Standard Agent Jobs
Create a database called spxBackupUtility this is used to house the stored procedures for the backup

Create a directory that will house the backups, either on the SQL Server or another server

Create a directory on SQL for log files from tasks, it can be anywhere I suggest no spaces in the path names just in case

Ensure no SQL Server Agent is running

Download MaintenanceSolution.sql and open in SSMS.

Right at the top of the file change

<pre>
USE [master] -- Specify the database in which the objects will be created.
</pre>

to
<pre>
USE [spxBackupUtility] -- The backup database we have created
</pre>

A little further down change the following
<pre>
SET @CreateJobs          = 'Y'                                  -- Specify whether jobs should be created.
SET @BackupDirectory     = N'C:\sql_restores'             -- Specify the backup root directory.
SET @OutputFileDirectory = N'C:\sql_restores\sql_maintenance_log_output'         -- Specify the output file directory. If no directory is specified, then the SQL Server error log directory is used.
SET @LogToTable          = 'Y'                                  -- Log commands to a table.
</pre>
Execute the script in SSMS


### Setup Database Mail

If this is a new server database mail won't be setup, so expand management -> Database Mail -> Create new account and setup for the umbee cloud emailer.

IMAGE


Test the email works by right clicking on database email and choosing send test email.

Now setup email as an alert system for SQL Agent.  Right click on SQL Agent and choose properties, then Alert System
Enable Mail system and choose Spidex

IMAGE

Now add netsupport address as an email operator, right click operators under SQL Agent and click New Operator

IMAGE

Set the name as Net Support and add the email address, the rest can stay as it is.


### Configure the Agent Jobs
The installed script sets up some standard agent jobs:-

IMAGE

By default these are not scheduled to run and need changes to their standard parameters, things like clean down times etc..

### Configure the Agent Jobs to notify by Email

Rick click the job to configure, choose properties and then choose notification, tick email and choose "Net Support", leave as 'if the job fails', click OK.

### Configure the Agent Jobs command execution with appropriate parameters

Rick click the job to configure, choose properties and then choose steps  
Click on the step that has been added by the script and click Edit  
If the parameters were set correctly in the initial script, the only thing to Edit ifor the DB Backups is Cleanup Time

SYSTEM DBS - FULL          :     120 Hours  
All the rest                       :     150 Hours

Index Optimise requires

DB Integrity checks require



### Schedule the Agent Jobs
Each Job needs to be scheduled otherwise it will not run:

Rick click on the job, choose properties and add schedules as listed below

SYSTEM DBS          Nightly at 3AM  
USER DBS - DIFF     Every night at 4AM apart from Friday  
USER DBS - FULL    Every Friday at 4AM  
USER DBS - LOG     Every Hour  

DB INTEG - SYSTEM          Sunday at 2.30AM  
DB INTEG - USERS            Sunday at 2AM  

INDEXES                              Sunday at 1AM  

From the recommendations on the SQL Maintenance Websites:-

### Scheduling
### How should I schedule jobs?
The answer depends on your maintenance window, the size of your databases, the maximum data loss you can tolerate, and many other factors. Here are some guidelines that you can start with, but you will need to adjust these to your environment.

User databases:

- Full backup one day per week
- Differential backup all other days of the week
- Transaction log backup every hour
- Integrity check one day per week
- Index maintenance one day per week
System databases:

- Full backup every day
- Integrity check one day per week
I recommend that you run a full backup after the index maintenance. The following differential backups will then be small. I also recommend that you perform the full backup after the integrity check. Then you know that the integrity of the backup is okay.

Cleanup:

- sp_delete_backuphistory one day per week
- sp_purge_jobhistory one day per week
- CommandLog cleanup one day per week
- Output file cleanup one day per week
