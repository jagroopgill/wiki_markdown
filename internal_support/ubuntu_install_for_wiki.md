# ubuntu install for Wiki

servername    ubwikitest

username       ubwikitestuser  
password        ub1234dfg%ABC


<b>wiki.js</b>

netsupport@spidex.co.uk
admin123


Install docker on ubuntu  
https://docs.docker.com/install/linux/docker-ce/ubuntu/


Then setup a network  
<pre>
sudo docker network create wikijs-network  
</pre>

Then setup a mongo container  

<pre>
docker pull mongo  
cd /  
sudo mkdir mongo  
sudo mkdir bin  
sudo docker run -d -v /data/bin:/data/bin   --network=wikijs-network --name wikimongo mongo  
</pre>

https://subscription.packtpub.com/book/big_data_and_business_intelligence/9781787126480/1/ch01lvl1sec18/running-mongodb-as-a-docker-container

<pre>
sudo docker run -d -v /data/bin:/data/bin -p 8080:3000 --name wikijs --network=wikijs-network wikijs  
</pre>

<b>bookstack</b>  

admin@admin.com  
password  

Add another network  
<pre>
sudo docker network create bookstack-network  
</pre>

Pull the mariadb image and get the GUID for the sudo user and the persitant file storage
<pre>
sudo docker pull linuxserver/mariadb

id ubwikitestuser  

sudo mkdir /data/mariadb  
</pre>

Setup the mariadb container  
<pre>
sudo docker create  --name=mariadb -e PUID=1000 -e PGID=1000 -e MYSQL_ROOT_PASSWORD=sysadminpass123! -e TZ=Europe/London -e MYSQL_DATABASE=user_db \
  -e MYSQL_USER=mysql_user_name -e MYSQL_PASSWORD=userpass123! -p 3306:3306 -v /data/mariadb:/config --network=bookstack-network --restart unless-stopped linuxserver/mariadb

sudo docker start mariadb
</pre>

Can check the process is now running and the logs
<pre>
sudo docker ps

sudo docker logs mariadb
</pre>



Open a bash shell on the mariadb container and setup the mysql db on the -p use the sysadmin password from above
<pre>
sudo docker exec -it mariadb bash

mysql -u root -p
CREATE DATABASE bookstackapp;
GRANT USAGE ON *.* TO 'myuser'@'%' IDENTIFIED BY 'userpass123!';
GRANT ALL privileges ON `bookstackapp`.* TO 'myuser'@'%';
FLUSH PRIVILEGES;
</pre>

Now create the bookstack container and attach to the same net

<pre>
sudo docker create --name=bookstack -e PUID=1000 -e PGID=1000 -e DB_HOST=mariadb -e DB_USER=myuser -e DB_PASS=userpass123! -e DB_DATABASE=bookstackapp -e APP_URL=spxwikitest.southeastasia.cloudapp.azure.com:8181 -p 80:8181 -v /data/bookstack:/config --restart unless-stopped --network=bookstack-network linuxserver/bookstack
</pre>



sudo docker ps

sudo docker logs bookstack


<b>Gollumn</b>

Install git

--setup git to commit back changes to the remote:-

https://grantwinney.com/hosting-a-github-wiki-remotely-on-ubuntu/

Create an ssh key for the repository

https://help.github.com/en/articles/connecting-to-github-with-ssh

<pre>
sudo -i
ssh-keygen -t rsa -b 4096 -C "hg@bitbucket.org"
</pre>

Passphrase - leave blank - press enter

Copy the rsa_id to ubwikitestuser

<pre>
sudo cp /root/.ssh/id_rsa ~/.ssh/id_rsa
sudo cp /root/.ssh/id_rsa.pub ~/.ssh/id_rsa.pub

sudo chown ubwikitestuser:ubwikitestuser id_rsa
</pre>

(use sysadmin for the password)

add the key pair to thespxnetsupport user on bitbucket

<pre>
tail ~/.ssh/id_rsa.pub
</pre>

and then the following not as sudo
<pre>
cd ~/.ssh
echo id_rsa.pub >> authorized_keys
</pre>
Check that both root and the standard user can ssh to bitbucket

<pre>
ssh -T hg@bitbucket.org
</pre>

Clone the wikitest-gollum repo
<pre>
cd /data
sudo mkdir wikitest-gollum
sudo chown ubwikitestuser:ubwikitestuser wikitest-gollum
git clone hg@bitbucket.org:spidexdev/wikitest-gollum.git
</pre>


Setup a global commit email/name and the upstream
<pre>
cd wikitest-gollum
git config --global user.email "netsupport@spidex.co.uk"
git config --global user.username "spxnetsupport"


git push --set-upstream origin master
git config --global push.default simple
</pre>

http://www.whiteboardcoder.com/2017/04/gollum-auto-sync-to-remote-git-repo.html
Now follow the last parts in this to setup an automatic hook to bitbucket

ad a file then use git push to check

then add a hook..
<pre>
nano .git/hooks/post-commit


</pre>

past this in..
<pre>
#!/bin/bash
#Simple push to origin master
git push origin master

</pre>

then make it executable..

<pre>
chmod x+a post-commit
</pre>

Then cd to the git repo and try the commit out..
<pre>
touch addanother.md
git status
git add addanother.md
git commit -m "Test commit comment"


</pre>
Now follow this

https://github.com/schnatterer/gollum-galore

move the wikitest-gollum so it's in /data/gollum/wiki

Create the Caddyfile from that link in /data/gollum

create the container...
<pre>
docker create --name=gollum -p8282:80 -e GOLLUM_PARAMS="--allow-uploads --live-preview" -e CADDY_PARAMS="-conf /gollum/Caddyfile" -v /data/gollum:/gollum schnatterer/gollum-galore
</pre>

## MIGHT NEED TO SORT GOLLUMNS GIT STUFF - Look at fixing gollumns git in here..
http://www.whiteboardcoder.com/2017/04/gollum-auto-sync-to-remote-git-repo.html


