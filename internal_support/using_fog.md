# Using FOG

## 1. Create an image
This sets up an image area for us to copy the image of a host into.  In this test am going to clone a full machine as a backup  
Click image management at the top  
Add a name  
Ensure you choose the new storage group that was created with the 150GB+ partition  
Choose the OS to be stored, and as this is a laptop it is a mutil-partition single disk (note it says not resizable but it actually is)

![Alt Text Here](fog1.PNG)  

## 2. Register a host
If you click on the host management icon then list all hosts none will display as none have been "registered" with fog yet

Setup your host to boot to PXE and boot it to PXE  
A fog management page will be displayed, choose Quick registration, allow the laptop to register and then turn it off when it reboots  
Now when you click list all hosts the host is listed, edit the host and change the name to something more appropriate than the mac address  

![Alt Text Here](fog2.PNG)  
 
Now we assign the image with the host, so edit the host once more and choose the image we created

![Alt Text Here](fog3.PNG)  

The one created was testWin7Full so choose that in the dropdown

## 3. Take an image from the host (upload)
Run fogprep on the machine to be backed up, shut it down.

Click task managment  
Click all hosts  
Choose the host that was registered  
Click upload image  
The task will now sit and wait until we boot the box..

When you boot the box it'll start to clone it, takes a while....

So far we have cloned a full copy of the machine, this can only be placed back onto the same machine, so it's a full backup so to speak.  
Now we need to sysprep the machine so that the image can be placed onto multiple devices


## 4. Sysprep the laptop
Install all your apps etc so it's ready to run.

sysprep /oobe /generalize /shutdown

This shuts the machine down.  
Create a new image testWin7sysprep  
Assign that image to the laptop that has been sysprep'd - note this means it needs to be registered on Fog first  
Create a task to upload that host  
startup the machine that was sysprep'd  
The image is copied to fog


