# Workstation Install Guide

Standard Workstation Installation  
Windows 10 pro  
Windows 10 administrator MUST be setup as spx_admin ask Nicole, James or Gary to enter the password see "Machine Local Admin" in Evernote  
Install Dell drivers from dell support  
The following list is an overview of the correct order to install drivers on Dell desktops and portables. Not all are required, if they don’t come up as being installed then ignore.  

This list is taken from here.  

- Desktop System Software (DSS) or Notebook System Software (NSS) - A vital utility that provides critical updates and patches for the operating system. If you are reinstalling Windows or updating all drivers, it is important that this software be installed first. This is located under theSystem and Configuration Utilities Category on the Drivers and Downloads page.
- Chipset - Helps Windows control system board components and controllers. This is located under the Chipset Category on the Drivers and Downloads page.
- PCMCIA/Smartcard controller located under the Security Category on the Drivers and Downloads page.
- Intel Storage - Intel Matrix Storage Manager located under SATA Drives on the Drivers and Downloads page (only for Intel chipset computers).
- Audio Adapter - Enables and enhances the audio controller. This is located under the Audio Category on the Drivers and Downloads page.
- Video Adapter - Enhances video performance. This is located under the Video Adapter Category on the Drivers and Downloads page.
- Network Interface Card (NIC) - Enhances the network controller for Internet or network access. This is located under the Network Category on the Drivers and Downloads page.
- Dell Control Point (DCP) - Controls power management, ambient light sensor, wireless profiles, and security features on laptops. This is located under the Applications on the Drivers and Downloads page. The DCP is split into three separate programs with each one controlling different aspects. It may not be necessary to install all three parts of DCP depending on the system configuration. 
- Control Point System Manager - Controls power management and ambient light sensor settings.
- Control Point Security Manager - Controls security features such as the trusted platform module a finger print reader.
- Control Point Connection Manager - Controls wireless connection profiles.
- Dell Control Point is new for Latitude E-Family. Other Dell laptops use Quickset to control power management and the ambient light sensor.
- Dell Quickset - Controls power management and the ambient light sensor. This is located under the Applications Category on the Drivers and Downloads page (Quickset is not applicable to Latitude E-Family laptops. Use Dell Control Point instead).
- Wireless Network Card - Enables and enhances the wireless network controller. This is located under the Network Category on the Drivers and Downloads page. DO NOT INSTALL – Use drivers from network$
- Bluetooth® Module located under the Communication Category on the Drivers and Downloads page.
- Touchpad, Pointer, Track stick, Mice, and Keyboards - Enhances the pointing device features. This is located under the Mouse & Keyboards Category on the Drivers and Downloads page.
### Other Devices
- Intel vPro™ or AMT™ - Enhances system manageability. This is located under the Chipset Category on the Drivers and Downloads page.
- Dell Wireless Mobile Broadband Cards located under the Communication Category on the Drivers and Downloads page
- ''''Modem - Allows dial-up capability. This is located under the Communication Category on the Drivers and Downloads page.
- Touch Screen Digitizer - Enables touch screen mouse control available on select Dell laptops. This is located under the Input Category on the Drivers and Downloads page.
- Configure the windows components
    - Add IIS (remove smtp from IIS)
- Change domain to SPIDEX (Nicole or Gary)
- Install Sophos firewall and virus (Nicole or Gary)
- Install applications
    - Adobe reader (do not install google toolbar)
    - ftp client
    - skype
    - notepad ++
    - pdf writer
    - zip tool
    - bitlocker
- Install Office
    - Configure inbox
- Install visio – check with Nicole if required
- Install project – check with Nicole if required
- Install Malware Bytes
- Install Printer
- Install Tortoise SVN (developers only)