# Spidex Interfaces



1.	Introduction

This document gives an insight on how Spidex uses Kettle to create and maintain customer interfaces.
In general, an interface extracts, transforms and loads data into a database or file.  Using Kettle is a simplified way of creating these interfaces and as you will find is a simple graphical user interface, with steps and arrows.  By following through the steps in a logical process you will understand how data is being extracted, transformed and then dealt with within each interface.

2.	Pre-Requisites

When using Kettle in the office, for instance to investigate a customer issues, you will need kettle installed; apply any scripts that are required for the interface, such as creating polled tables or triggers; and the latest pdi files which can be downloaded from SVN.
Once you have downloaded the files from SVN, you will need to do the following:
a)	Set-up the parameter file with the correct database connections.
b)	Point the first step in the first interface file that gets called to the correct directory.
c)	Change the kitchen commands to point to the correct directories.

3.	Transformations, Jobs and Error Handling

Most of Spidex interfaces generally consist of a set of transformations and jobs.
The general structure of an interface consists of connections to any databases are established initially.  This is done by retrieving the database details from a parameter file.  If they are up and running then continue with the procedure.  Check whether there are any records that need processing.  If not, stop the procedure, else continue.
 
The example above shows the interface files being located in the tSetDir step, followed by checking each database connection, and then calling another job to see whether there is anything that needs to be processed.
This is the jStxnsChkRows job (see below) which checks whether there is a successful database connection, and whether there are any rows that need processing.  If there are rows to be processed it will continue to run the jStxns job which will include the main transformation that does the processing.
 

The main transformation in a job can be identified with the naming convention ……Proc.ktr.  This generally contains the main process of the job.  For instance, extracting information, transforming data or updating information in a database.
The example below is taken from Global Renewables interface.
 
A few things to note from the example above:
1)	The flow in the transformation from where you can see a red dotted arrow, is to do with the error handling part of the process.  If an error occurs at any of the steps it is leading from the error will be handled accordingly.
2)	The getRows step is an example of the interface retrieving information from the database (in this case Mainsaver).
3)	The chgFieldLengths step is an example of taking the retrieved information and changing it to meet the requirements for the insert later on.
4)	The insStgTable is taking the retrieved information, which may have been altered during the flow and is then inserted back into a polled table in another database.
5)	The orange notes displayed will provide further information about what each step is doing throughout the process.
If any errors occur, they will be handled and will be reported to an error log which in most cases is set to be e-mailed to a specific person, or group.

4.	File Directory Structure

All the interfaces have a similar directory structure.  Under the pdi_files directory you should find a directory similar to the following:
 
1)	Csv files containing database connections.  In the example above they are named db_settings (Mainsaver database details) and stage_settings (staging database details).  In recent interfaces these can usually be found in a separate folder, usually called “parameters”.
2)	A “reused” folder (or a folder with a similar name). This folder generally contains all the jobs and transformations that are continually reused throughout the interface, for instance setting database connections or mailing a file.
3)	Each process in the interface tends to have its own folder for all the jobs and transformations associated with that process.  For instance, in the example above the “cc” folder has all the files that are to do with importing cost centres, the “supp” folder has all the files associated with the supplier import and the “stxns” folder has all the files associated with the stock transaction extract and so on.
So when dealing with an interface issue and the issue is something to do with the supplier import, you will probably check out the files in the “supp” folder first to help identify the problem.
In most cases you should be able to identify what each folder represents, but in some cases, for instance Wiseman and ABP where the interface is quite complex, you may have to check a few folders out to determine the correct process you are looking at.  You could also download the latest technical spec from SVN which also helps identifying what each process is meant to do.
5.	Environment Variables

Environment variables are used throughout interfaces as it is a useful feature when designing transformations for testing variable substitutions that are normally set dynamically by another job or transformation.  For instance when setting database connection details rather than hardcoding each transformation or job with connection details, the connection details are picked up once from a csv file, usually named db_settings.csv or something similar, set as environment variables, which can then be used for every transformation or job used throughout the interface.

 

You can use Spoon for debugging using the Preview button

6.	Kitchen Commands

These are the commands that are used to run each process.  They are used to set up jobs to run automatically in the scheduler as well as running the job manually through the command prompt.
Example:
kitchen.bat /file:"C:\Code\GR\pdi_files\cc\jCCSetup.kjb" "C:\Code\GR\pdi_files" "C:\Code\GR"
Open the command prompt and navigate to the directory where Kettle is installed.  Then enter the command above.  Anything in double quotes are parameters that are being sent.  So in this case the first parameter calls the main job for the process, so just ensure that this is pointing to the correct directory on your machine, or server.
The following parameters are sent to the job, in this case the first parameter identifies where all the interface files are located and the second identifies the data files, for instance if any files are retrieved or created they will be located in this directory.

7.	Scheduler

The scheduler can be found on the network usually at this location:
J:\Apps\Pentaho\scheduler
After installing the scheduler you will be able to set up interface jobs to run automatically.  This is installed at customer sites along with the interface files.
To set-up the scheduler you need to run the jobeditor.cmd which can be found in the following location:
C:\ProgramData\sos-berlin.com\jobscheduler\scheduler\bin\jobeditor.cmd
You can usually double-click on jobeditor.cmd, but if nothing happens you will need to open this using the command prompt.  Navigate to the jobeditor.cmd directory and set JAVA_HOME in the command prompt to point to Java (you just need to ensure that the directory that you point to does NOT have any spaces in the path).
If you use the jobeditor.cmd for the first time, ensure you create a copy of the scheduler.xml which can be found in the following location:
C:\Program Files\sos-berlin.com\jobscheduler\scheduler\bin\
All the settings can be found in the scheduler.xml file which you can open in the jobeditor by using File > Open and navigating to:
       C:\ProgramData\sos-berlin.com\jobscheduler\scheduler\config\scheduler.xml
You can find scheduler logs in the following location:
C:\ProgramData\sos-berlin.com\jobscheduler\scheduler\logs
They can provide invaluable information on why a job hasn’t processed successfully, or whether any errors have occurred.  The logs that usually provide the most information have the name ‘job’ in the filename and this shows when the job has been run.  Also filenames containing ‘task’ in the filename are useful and these show a detailed log of a particular job, which again can be identified from the filename.
Sometimes if an error has occurred, the scheduler will stop running the job.  You can check the log files for that particular job, after which you can stop and restart the scheduler in Services, which will allow the job to run again.  
