Starting an Oracle Instance

Before importing or exporting a database, ensure the Oracle instance is started. This can be done by opening a command prompt by selecting Start > Run and entering ‘cmd’

Enter the following in the command prompt:

set ORACLE_SID=ORCL

Followed by:

sqlplus /nolog

At the SQL prompt enter:

connect sys/change_on_install as sysdba

Then:

startup


Dropping an existing database

If you already have a copy of a database on your machine, you can use a script to drop all Mainsaver objects ready to import another Mainsaver database onto your machine.

The script can be found in the following location and needs to be copied to a local directory:

  J:\Apps\oracle\oracle scripts\drop_msvr
Once you have copied the file across you will need to run the script.

Open a command prompt and enter:

sqlplus msdb1/msdb1

Then run the script by entering:

@C:\drop_msvr

At the prompt press Enter, and then enter the schema name as msdb1.

Once the script has completed the screen will display procedure completed successfully

You can then continue to import a database.


Importing an Oracle database

In the command prompt enter:

Set ORACLE_SID=ORCL

Navigate to the directory in which the import file and dmp file is stored as follows:

cd \import_export

And then run the following command:

imp system/manager parfile=imp_msvr.imp

Contents of imp_msvr.imp file (which can also be found on the network:J:\Apps\oracle\oracle scripts\import_export):

  fromuser=msdb  
  touser=daa  
  file=msdb.dmp  
  ignore=y  
  rows=y  
  buffer=10240000  
  log=msdb.log  
  grants=y  

Once this has completed the database has been imported. You will then need to continue to the orasyn.sql script on each user on the database.

Save the orasyn.sql script on a local directory and enter the following command into a command prompt:

Set ORACLE_SID=ORCL

Continue to login to SQL plus as follows:

Sqlplus msdb1/msdb1

Next run the script by typing the following command:

@C:\ORASyn

You will need to login as each user and run this script for each user.


Exporting an Oracle Database

Ensure the Oracle instance is started and uploaded with a database.

In the command prompt navigate to the import_export directory:

cd \import_export

Then in the command prompt enter the following command:

Exp system/manager parfile=msdb1.exp

And the export should start and a dmp file of the database will be created in the import_export directory.

Contents of msdb1.exp file (which can also be found on the network:J:\Apps\oracle\oracle scripts\import_export):

  owner=msdb1  
  buffer=1042000  
  file=msdb1.dmp  
  compress=n  
  statistics=none  
  log=msdb1_exp.log  
  
Additional Oracle information can be found in the following location on the network:

  K:\Helpdesk\Info

How to unlock an Oracle account

Login to SQL plus as follows: sqlplus sys/orange98 as sysdba (in this case you can use any password as it uses windows authentication to login)

Enter: alter user system account unlock;


How to change the password of user including system password

Login to SQL plus as sys user

Enter: alter user system identified by oracle;

(The above command changes the password to 'oracle')


How to run a trace on an Oracle DB

To run a trace on an Oracle database you can ammend the mainsave.ini file to include the following (note the keyword trace after dbms):

   datasetname=ORCL10G  
   entdatasource=  
   dbms=trace O10  
   dbname=Oracle  
   servername=ORCL10G  
   Dbparm=staticbind=0, disablebind=1  

When you next open Mainsaver it will ask you where you would like to create your trace file.