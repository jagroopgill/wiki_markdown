# Importing an Oracle database
Ensure the Oracle instance is started.  This can be done by opening a command prompt by selecting Start > Run and entering ‘cmd’  

Enter the following in the command prompt: 

set ORACLE_SID=ORCL  

Followed by:  

sqlplus /nolog  

At the SQL prompt enter:  

connect sys/change_on_install as sysdba  

Then:  

startup  

As demonstrated below:  
 
 IMAGE

If you already have a copy of a database on your machine, you can use a script to drop all Mainsaver objects ready to import another Mainsaver database onto your machine.

The script can be found in the following location and needs to be copied to a local directory:

J:\Apps\oracle\drop_msvr

Once you have copied the file across you will need to run the script.

Open a command prompt and enter:

sqlplus msdb1/msdb1

Then run the script by entering:

@C:\drop_msvr
 
At the prompt press Enter, and then enter the schema name as ‘msdb1’.

Once the script has completed the screen will appear as shown below:
 
You can then continue to import a database.  

You will require the following files located here:
J:\Apps\oracle\import_export

Create a directory called something similar (like import_export) on your C drive and copy the files across from the location above.

In the same directory copy across the dmp file which you would like to import.

If you open the imp_msvr.imp file the name of the dmp file that needs to be imported and the name of the file in the import file need to be the same.

Then you can continue to import the dmp file by opening a command prompt and entering the following command:

Set ORACLE_SID=ORCL

Navigate to the directory in which the import file and dmp file is stored as follows:

cd \import_export

And then run the following command:

imp system/manager parfile=imp_msvr.imp

Once this has completed the database has been imported.  You will then need to continue to the orasyn.sql script on each user on the database.

Save the orasyn.sql script on a local directory and enter the following command into a command prompt:

Set ORACLE_SID=ORCL

Continue to login to SQL plus as follows:

Sqlplus msdb1/msdb1
 
Next run the script by typing the following command:

@C:\ORASyn

You will need to login as each user and run this script for each user.

Once the database has been imported and the necessary scripts have been run, you may wish to create a back-up of the database.

In order to do this, shutdown the Oracle instance by entering the following commands:

Sqlplus /nolog

Conn sys/change_on_install as sysdba

Shutdown immediate
 
Then zip up the database files found in the following location:

C:\oracle\oradata

Once the files have been zipped up you can restart the database by entering ‘startup’ in the SQL prompt.

### Exporting a database

Ensure the Oracle instance is started and a uploaded with a database.

In the command prompt navigate to the import_export directory:

cd \import_export 

Then in the command prompt enter the following command:

Exp system/manager parfile=msdb1.exp

And the export should start and a dmp file of the database will be created in the import_export directory.




